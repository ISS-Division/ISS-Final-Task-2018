package it.unibo.custom.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import it.unibo.qactors.akka.QActor;

public class hueRealMock {
	
	private static final String PUT_URL = "http://<bridge ip address>/api/<user id authorized>/lights/1/state";
	private static hueRealMock currenthueRealMock = null;
	private static QActor actor;
	
	public hueRealMock(QActor myActor) {
    	actor = myActor;
	}
	
	public static synchronized hueRealMock createhueRealMock(QActor myActor) { //Factory method
    	if (currenthueRealMock == null ) currenthueRealMock = new hueRealMock(myActor);
    	return currenthueRealMock;
    }
	public static void sendPUT(QActor myActor, String state) throws IOException {
		URL obj = new URL(PUT_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("PUT");
		int responseCode = con.getResponseCode();
		OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
		if(state.equalsIgnoreCase("on")) {
			out.write("	{\"on\":true}");
		}else {
			out.write("	{\"on\":false}");
		}
		System.out.println("PUT Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			System.out.println("PUT request worked");
		} else {
			System.out.println("PUT request not worked");
		}
	}
}
