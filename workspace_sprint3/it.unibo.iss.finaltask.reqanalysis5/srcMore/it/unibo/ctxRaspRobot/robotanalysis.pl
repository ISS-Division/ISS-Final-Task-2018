%====================================================================================
% Context ctxRaspRobot  SYSTEM-configuration: file it.unibo.ctxRaspRobot.robotanalysis.pl 
%====================================================================================
context(ctxfinaltask, "localhost",  "TCP", "8570" ).  		 
context(ctxrasprobot, "localhost",  "TCP", "8571" ).  		 
%%% -------------------------------------------
qactor( realrobot , ctxrasprobot, "it.unibo.realrobot.MsgHandle_Realrobot"   ). %%store msgs 
qactor( realrobot_ctrl , ctxrasprobot, "it.unibo.realrobot.Realrobot"   ). %%control-driven 
qactor( testsonarreal , ctxrasprobot, "it.unibo.testsonarreal.MsgHandle_Testsonarreal"   ). %%store msgs 
qactor( testsonarreal_ctrl , ctxrasprobot, "it.unibo.testsonarreal.Testsonarreal"   ). %%control-driven 
qactor( sonartocollision , ctxrasprobot, "it.unibo.sonartocollision.MsgHandle_Sonartocollision"   ). %%store msgs 
qactor( sonartocollision_ctrl , ctxrasprobot, "it.unibo.sonartocollision.Sonartocollision"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(sonarevh,ctxrasprobot,"it.unibo.ctxRaspRobot.Sonarevh","sonarEvent").  
%%% -------------------------------------------

