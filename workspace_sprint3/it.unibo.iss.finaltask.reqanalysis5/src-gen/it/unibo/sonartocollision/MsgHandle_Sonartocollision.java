/* Generated by AN DISI Unibo */ 
package it.unibo.sonartocollision;
import it.unibo.qactors.QActorContext;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.akka.QActorMsgQueue;

public class MsgHandle_Sonartocollision extends QActorMsgQueue{
	public MsgHandle_Sonartocollision(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  {
		super(actorId, myCtx, outEnvView);
	}
}
