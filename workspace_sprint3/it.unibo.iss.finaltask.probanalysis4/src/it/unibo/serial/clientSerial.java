package it.unibo.serial;

import it.unibo.qactors.akka.QActor;
import jssc.SerialPort;

public class clientSerial {

	private static SerialPortConnSupport serialConn = null;
	//In need change the port
	private static String port ="/dev/ttyACM0";
	private static SerialPort serialPort;
	private static SerialPortSupport serialPortSup;
	private static final int collisionThreshold = 2;
	
	public static void initClientConn(QActor qa) throws Exception {
		serialPortSup = new SerialPortSupport(qa.getOutputView());
		serialPort = serialPortSup.connect(port);
		serialConn = new SerialPortConnSupport(serialPort, qa.getOutputView());
		System.out.println("Serial conneciton init");
		
	}

	public static void realRobotCommand(QActor qa, String cmd, String time, String power) throws Exception {
		String command = "";
		char c = cmd.charAt(0);
		command += c + "," + time + "," + power;
		//System.out.println("SENDING TO ARDUINO "+command);
		serialConn.sendALine(command);
	}

	public static void readSonar(final QActor qa) {
		new Thread() {
			public void run() {
				while (true) {
					try {
						String distance = serialConn.receiveALine().trim();
						int d = Integer.parseInt(distance);
						if (d < collisionThreshold) {
							qa.emit("collisionEvent",
									"collision(TARGET)".replace("TARGET","undefindObject"));
							System.out.println("Detected real robot collision");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

}
