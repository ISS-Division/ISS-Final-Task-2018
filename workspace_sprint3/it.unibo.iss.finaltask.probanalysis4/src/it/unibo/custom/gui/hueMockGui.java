package it.unibo.custom.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import it.unibo.qactors.akka.QActor;

public class hueMockGui extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private static hueMockGui currentHueGui = null;
	public static Color HueColor = Color.RED;
	public static Color currentHueColor = Color.WHITE; 
	public static final boolean HueGuiOn  = true;
	public static final boolean HueGuiOff = false;
	public static boolean HueStatus = HueGuiOff;
	public static JFrame frame = new JFrame();

	public static synchronized hueMockGui createCustomHueGui(QActor myActor) { //Factory method
    	if (currentHueGui == null ) currentHueGui = new hueMockGui(myActor);
    	return currentHueGui;
    }
	
    public static void setHue(QActor myActor, String value) {
      	currentHueGui.setHueGui( value.equals("on") ? HueGuiOn : HueGuiOff );
    }   
    
    public hueMockGui(QActor myActor) {
    	final JColorChooser colorChooser = new JColorChooser(Color.RED);
    	ColorSelectionModel model = colorChooser.getSelectionModel();
        ChangeListener changeListener = new ChangeListener() {
          public void stateChanged(ChangeEvent changeEvent) {
            Color newColor = colorChooser.getColor();
            HueColor = newColor;
          }
        };
        colorChooser.setPreviewPanel(new JPanel());
        model.addChangeListener(changeListener);
        frame.add(colorChooser, BorderLayout.SOUTH);
        frame.setTitle("hueMockGui");
        frame.add(this);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700, 700);
        frame.setVisible(true);
    }
	
    public void paintComponent(Graphics g) {
        if(HueStatus)
            currentHueColor = HueColor;
        else
            currentHueColor = Color.WHITE;
        g.setColor(currentHueColor);
        g.fillOval(frame.getWidth()/2-125, 50, 250, 250);
    }

    public void setHueGui(boolean status) {
    	HueStatus = status;
    	repaint();
    } 
    
    
    // For testing
    public static void main(String[] args) throws InterruptedException
    {
        hueMockGui hue = new hueMockGui(null);
        for( int i=1; i<=10; i++) {
			Thread.sleep(1000);
			hue.setHueGui(HueGuiOn);
			Thread.sleep(1000);
			hue.setHueGui(HueGuiOff);
    	}
    }
	
}
