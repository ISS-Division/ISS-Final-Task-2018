package it.unibo.exploremap.program;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import aima.core.agent.Action;
import aima.core.search.framework.SearchAgent;
import aima.core.search.framework.problem.GoalTest;
import aima.core.search.framework.problem.Problem;
import aima.core.search.framework.qsearch.GraphSearch;
import aima.core.search.uninformed.BreadthFirstSearch;
import aima.core.search.uninformed.DepthFirstSearch;
import it.unibo.exploremap.model.Box;
import it.unibo.exploremap.model.Functions;
import it.unibo.exploremap.model.RobotAction;
import it.unibo.exploremap.model.RobotState;
import it.unibo.exploremap.model.RobotState.Direction;
import it.unibo.exploremap.model.RoomMap;
import it.unibo.utils.SoffrittiTCP;

public class Program {
	private static boolean wallDownFound = false;
	private static boolean wallRightFound = false;
	private static RobotState initialState = new RobotState(0,0,RobotState.Direction.DOWN);
	
	private static boolean goToSonar1 = false;
	
	public static void main(String[] args) {
		try {
			SoffrittiTCP.initConnTest("localhost", 8999);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		initialState = new RobotState(0, 0, RobotState.Direction.DOWN);
		clean();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Going back to sonar1");
		goToSonar1();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Cleaning again");
		RoomMap.getRoomMap().setDirty();
		clean();
		try {
			SoffrittiTCP.soffrittiListeningThread.stop();
			SoffrittiTCP.localCollisionSocket.close();
			SoffrittiTCP.localSendCollisionSocket.close();
			SoffrittiTCP.localSonarSocket.close();
			SoffrittiTCP.localSendSonarSocket.close();
			SoffrittiTCP.localServerSocket.close();
			SoffrittiTCP.clientSocket.close();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void resetAll() {
		RoomMap.getRoomMap().resetMap();

		wallDownFound = false;
		wallRightFound = false;
		initialState = new RobotState(0,0,RobotState.Direction.DOWN);
		goToSonar1 = false;
	}
	
	public static void goToSonar1() {
		goToSonar1 = true;
		clean();
		goToSonar1 = false;
	}
	
	@SuppressWarnings("deprecation")
	public static void clean() {
		
		System.out.println(RoomMap.getRoomMap().toString());
		
		BreadthFirstSearch search = new BreadthFirstSearch(new GraphSearch());
//		DepthFirstSearch search = new DepthFirstSearch(new GraphSearch());
		SearchAgent searchAgent;
		for (int k=0; k<Integer.MAX_VALUE; k++) {
			try {
				List<Action> actions;
				GoalTest goalTest;
				if (!goToSonar1) {
					if (RoomMap.getRoomMap().isClean() && wallDownFound && wallRightFound) {
						if (initialState.getX() == RoomMap.getRoomMap().getDimX()-2 &&
								initialState.getY() == RoomMap.getRoomMap().getDimY()-2) {
							System.out.println("FINISHED");
							break;
						}
						System.out.println("Finished cleaning, going to sonar2");
						goalTest = new GoalTest() {
							
							@Override
							public boolean isGoalState(Object state) {
								RobotState robotState = (RobotState) state;
								if (robotState.getX() == RoomMap.getRoomMap().getDimX()-2 &&
										robotState.getY() == RoomMap.getRoomMap().getDimY()-2 && robotState.getDirection() == Direction.RIGHT)
									return true;
								else
									return false;
							}
						};
					} else
						goalTest = new Functions();
					Problem problem = new Problem(initialState, new Functions(), new Functions(), goalTest, new Functions());
					if (wallDownFound && !wallRightFound) {
						actions = new ArrayList<>();
						if (initialState.getDirection() == Direction.DOWN)
							actions.add(new RobotAction(RobotAction.TURNLEFT));
						actions.add(new RobotAction(RobotAction.FORWARD));
					} else {
						searchAgent = new SearchAgent(problem, search);
						actions = searchAgent.getActions();
						if (actions == null || actions.isEmpty()) {
							if (!RoomMap.getRoomMap().isClean())
								RoomMap.getRoomMap().setObstacles();
							actions = new ArrayList<Action>();
						}
					}
					if ((actions == null || actions.isEmpty()) && (!wallDownFound || !wallRightFound))
						break;
				} else {
					Problem problem = new Problem(initialState, new Functions(), new Functions(), new GoalTest() {

						@Override
						public boolean isGoalState(Object state) {
							RobotState robotState = (RobotState) state;
							if (robotState.getX() == 0 &&
									robotState.getY() == 0 && robotState.getDirection() == Direction.DOWN)
								return true;
							else
								return false;
						}
						
					}, new Functions());
					searchAgent = new SearchAgent(problem, search);
					actions = searchAgent.getActions();
					if (actions == null || actions.isEmpty() || actions.get(0).isNoOp())
						break;
				}
				System.out.println("Step " + k + "\n" + actions);
				for (Action ac : actions) {
					if (ac.isNoOp()) {
						System.out.println("Can't move from here");
						return;
					}
					RobotAction a = (RobotAction) ac;
//					try {
//						//Flush input
//						while(true) {
//							System.out.println("Main:" + SoffrittiTCP.getSoffrittiSonars().readLine());
//						}
//					} catch (SocketTimeoutException e) {
//						System.out.println("Sonar buffer is now empty");
//					}
					moveRobot(a);
					if (a.getAction() == RobotAction.FORWARD) {
						long time = System.currentTimeMillis();
						String input;
						try {
							//Timing erros, this is needed to synchronize the position on the 4 sides
							if (initialState.getX() == 1 && initialState.getDirection() == Direction.LEFT ||
								initialState.getY() == 1 && initialState.getDirection() == Direction.UP ||
								(wallDownFound && initialState.getY() == RoomMap.getRoomMap().getDimY()-3 && initialState.getDirection() == Direction.DOWN) ||
								(wallRightFound && initialState.getX() == RoomMap.getRoomMap().getDimX()-3 && initialState.getDirection() == Direction.RIGHT) ) {
								System.out.println("Position synchronized with wall");
								Thread.sleep(350);
								moveRobot(a); //Needed for bad timings
								Thread.sleep(350);
								try {
									//Flush input
									while(true) {
										SoffrittiTCP.getSoffrittiCollisions().readLine();
									}
								} catch (SocketTimeoutException e) {
									
								}
								RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, false));
								initialState = (RobotState) new Functions().result(initialState, a);
								RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, true)); 
								continue;
							}
							input = SoffrittiTCP.getSoffrittiCollisions().readLine();
							time = System.currentTimeMillis() - time;
							System.out.println("Collision detected");
							moveRobot(new RobotAction(RobotAction.BACKWARD), (int)time);
							Thread.sleep(300);
							Thread.sleep(2000); //Mobile obstacle?
							moveRobot(new RobotAction(RobotAction.FORWARD), 300);
							time = System.currentTimeMillis();
							input = SoffrittiTCP.getSoffrittiCollisions().readLine();
							time = System.currentTimeMillis() - time;
							RobotState temp = (RobotState) new Functions().result(initialState, a);
							RoomMap.getRoomMap().put(temp.getX(), temp.getY(), new Box(true, false));
							System.out.println("Fixed obstacle, new plan...");
							moveRobot(new RobotAction(RobotAction.BACKWARD), (int)time);
							Thread.sleep(time);
							
							//If there is a sonar event from sonar2
							if (!wallDownFound && initialState.getDirection() == Direction.DOWN) {
								Thread.sleep(1000); //Wait for the sonar event to arrive
								try {
									String sonarEv = SoffrittiTCP.getSoffrittiSonars().readLine();
									System.out.println("WallDown: " + sonarEv);
									while (!sonarEv.contains("sonar2")) {
										sonarEv = SoffrittiTCP.getSoffrittiSonars().readLine();
										System.out.println("WallDown: " + sonarEv);
									}
									
									//Sonar2 detected, update map
									for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
										RoomMap.getRoomMap().put(i, initialState.getY()+1, new Box(true, false, false));
									}
									wallDownFound = true;
									//Map changed, replan
									System.out.println("WallDown found, replan...");
									break;
								} catch (SocketTimeoutException e) {
									//No sonar2, go ahead
									System.out.println("No sonar2");
								}
							}
							else if (wallDownFound && !wallRightFound && initialState.getDirection() == Direction.RIGHT) {
								for (int i=0; i<RoomMap.getRoomMap().getDimY(); i++) {
									RoomMap.getRoomMap().put(initialState.getX()+1, i, new Box(true, false, false));
								}
								wallRightFound = true;
								//Map changed, replan
								System.out.println("WallRight found, replan...");
								break;
							}
							
							Thread.sleep(300);
							break;
						} catch (SocketTimeoutException e) {
							System.out.println("No collision");
							RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, false));
//							System.out.println(RoomMap.getRoomMap().toString());
							initialState = (RobotState) new Functions().result(initialState, a);
							RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, true));
							
							//If there is a sonar event from sonar2
							if (wallDownFound && !wallRightFound && initialState.getDirection() == Direction.RIGHT) {
								for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
									RoomMap.getRoomMap().put(i, initialState.getY()+1, new Box(true, false, false));
								}
								wallDownFound = true;
								//Map changed, replan
								System.out.println("WallDown found, replan...");
								break;
							}
							
//							Thread.sleep(500);
						}
						
					} else {
						initialState = (RobotState) new Functions().result(initialState, a);
						Thread.sleep(500);
					}
						
				}
				System.out.println(RoomMap.getRoomMap().toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(RoomMap.getRoomMap().toString());
		System.out.println("Algorithm end");
	}
	
	public static void moveRobot(RobotAction a) throws Exception {
		switch(a.getAction()) {
		case RobotAction.FORWARD: SoffrittiTCP.soffrittiCommand("moveForward", "300"); break;
		case RobotAction.BACKWARD: SoffrittiTCP.soffrittiCommand("moveBackward", "300"); break;
		case RobotAction.TURNLEFT: SoffrittiTCP.soffrittiCommand("turnLeft", "500"); break;
		case RobotAction.TURNRIGHT: SoffrittiTCP.soffrittiCommand("turnRight", "500"); break;
		default: System.out.println("Error, action unrecognized");
		}
	}
	
	public static void moveRobot(RobotAction a, int time) throws Exception {
		switch(a.getAction()) {
		case RobotAction.FORWARD: SoffrittiTCP.soffrittiCommand("moveForward", ""+time); break;
		case RobotAction.BACKWARD: SoffrittiTCP.soffrittiCommand("moveBackward", ""+time); break;
		case RobotAction.TURNLEFT: SoffrittiTCP.soffrittiCommand("turnLeft", ""+time); break;
		case RobotAction.TURNRIGHT: SoffrittiTCP.soffrittiCommand("turnRight", ""+time); break;
		default: System.out.println("Error, action unrecognized");
		}
	}
}
