package it.unibo.utils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.json.JSONObject;

public class SoffrittiTCP   {
private static String sep      = ";";
public static Socket clientSocket ;
protected static PrintWriter outToServer;
protected static BufferedReader inFromServer;
public static ServerSocket localServerSocket ;
public static Socket localSendCollisionSocket ;
public static Socket localCollisionSocket ;
public static Socket localSendSonarSocket;
public static Socket localSonarSocket;
public static Thread soffrittiListeningThread;

	public static BufferedReader getSoffrittiCollisions() throws UnknownHostException, IOException {
		return new BufferedReader(new InputStreamReader(localCollisionSocket.getInputStream()));
	}
	
	public static BufferedReader getSoffrittiSonars() throws UnknownHostException, IOException {
		return new BufferedReader(new InputStreamReader(localSonarSocket.getInputStream()));
	}
	
	public static void reinitSoffritti(String hostName, int port) throws Exception {
		soffrittiListeningThread.stop();
		if (clientSocket != null) {
			clientSocket.close();
			clientSocket = new Socket(hostName, port);
//			clientSocket.setSoTimeout(300);
			inFromServer = new BufferedReader( new InputStreamReader(clientSocket.getInputStream()) );  
			outToServer  = new PrintWriter(clientSocket.getOutputStream());
		}
		startTheReader();
	}

	public static void initConnTest(String hostName, int port) throws Exception {
		 clientSocket = new Socket(hostName, port);
//		 clientSocket.setSoTimeout(300);
		 inFromServer = new BufferedReader( new InputStreamReader(clientSocket.getInputStream()) );  
		 outToServer  = new PrintWriter(clientSocket.getOutputStream());

		 localServerSocket = new ServerSocket(10002);
//		 localServerSocket.setReuseAddress(true);
		 localSendCollisionSocket = new Socket("localhost", 10002);
		 localCollisionSocket = localServerSocket.accept();
		 localCollisionSocket.setSoTimeout(300);
//		 new PrintWriter(localSendSocket.getOutputStream(), true).println("ciao");
//		 localSendSocket.close();
//		 System.out.println("fatto");
//		 String line = new BufferedReader (new InputStreamReader(localCollisionSocket.getInputStream())).readLine();
//		 System.out.println(line);
		 
		 localSendSonarSocket = new Socket("localhost", 10002);
		 localSonarSocket = localServerSocket.accept();
		 localSonarSocket.setSoTimeout(500);
		 
		 startTheReader();
	}
	public static void sendMsg(String jsonString) throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		String msg = sep+jsonObject.toString()+sep;
		outToServer.println(msg);
		outToServer.flush();
	}
	public static void soffrittiCommand(String command, String time) throws Exception {
		sendMsg("{ 'type': '"+command+"' , 'arg': "+time+" }");
	}
 	protected static void startTheReader() {		
		soffrittiListeningThread = new Thread() {
			public void run() {
				try {
					PrintWriter sendCollisionWriter = new PrintWriter(localSendCollisionSocket.getOutputStream(), true);
					PrintWriter sendSonarWriter = new PrintWriter(localSendSonarSocket.getOutputStream(), true);
					while( true ) {				 
						try {
							String inputStr = inFromServer.readLine();
							//System.out.println( "reads: " + inpuStr);
							String jsonMsgStr = inputStr.split(";")[1];
							//System.out.println( "reads: " + jsonMsgStr + " qa=" + qa.getName() );
							JSONObject jsonObject = new JSONObject(jsonMsgStr);
							//System.out.println( "type: " + jsonObject.getString("type"));
							switch (jsonObject.getString("type") ) {
//							case "webpage-ready" : System.out.println( "webpage-ready "   );break;
							case "sonar-activated" : {
								//wSystem.out.println( "sonar-activated "   );
								JSONObject jsonArg = jsonObject.getJSONObject("arg");
								String sonarName   = jsonArg.getString("sonarName");							
								int distance       = jsonArg.getInt( "distance" );
//								System.out.println( "sonarName=" +  sonarName + " distance=" + distance);
//								qa.emit("sonarEvent", 
//									"sonar(NAME, DISTANCE)".replace("NAME", sonarName.replace("-", "")).replace("DISTANCE", (""+distance) ));
								
								//Send sonar to AI MAP
								sendSonarWriter.println(sonarName + ", " + distance);
								System.out.println(inputStr);
								break;
							}
							case "collision" : {
								//System.out.println( "collision"   );
								JSONObject jsonArg  = jsonObject.getJSONObject("arg");
								String objectName   = jsonArg.getString("objectName");
//								System.out.println( "collision objectName=" +  objectName  );
//								qa.emit("collisionEvent",
//										"collision(TARGET)".replace("TARGET", objectName.replace("-", "")));
								//Send collision to AI MAP
								System.out.println(inputStr);
								sendCollisionWriter.println(inputStr);
								break;
							}
							};
	 					} catch (IOException e) {
	 						e.printStackTrace();
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			
			}
		};
		soffrittiListeningThread.start();
	}

}
