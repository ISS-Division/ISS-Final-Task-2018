package it.unibo.exploremap.tests;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.SocketException;

import org.junit.Test;
import org.junit.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import it.unibo.exploremap.model.RoomMap;
import it.unibo.exploremap.program.Program;
import it.unibo.utils.SoffrittiTCP;

public class MapTests {
	
	private Process soff;
	private WebDriver chrome;
	private Dimension dim = new Dimension(400, 400);
	private Point pos = new Point(800, 0);
	
//	@Rule
//	public TestWatcher failWatcher = new TestWatcher() {
//		@Override
//	    protected void finished(Description description) {
//			soff.destroy();
//	        chrome.close();
//	    }
//	};
	
	@BeforeClass
	public static void initInternalConn() {
		try {
			SoffrittiTCP.initConnTest("localhost", 8999);
		} catch (Exception e) {
			//Should not be a problem
		}
	}
	
	
	@Before
	public void setUpSoffritti() throws SocketException {
		//Empty input buffers
		int to = SoffrittiTCP.localCollisionSocket.getSoTimeout();
		try {
			SoffrittiTCP.localCollisionSocket.setSoTimeout(1);
			while(true) {
				SoffrittiTCP.getSoffrittiSonars().readLine();
			}
		} catch (Exception e) {
			//Sonar buffer empty
		}
		SoffrittiTCP.localCollisionSocket.setSoTimeout(to);
		to = SoffrittiTCP.localSonarSocket.getSoTimeout();
		try {
			SoffrittiTCP.localSonarSocket.setSoTimeout(1);
			while(true) {
				SoffrittiTCP.getSoffrittiCollisions().readLine();
			}
		} catch (Exception e) {
			//Collision buffer empty
		}
		SoffrittiTCP.localSonarSocket.setSoTimeout(to);
		
		String os = System.getProperty("os.name");
		try {
			if (os.toLowerCase().contains("windows")) {
				soff = Runtime.getRuntime().exec("cd .. \n cd .. \n cd Soffritti \n startServer.bat");
			} else {
				String[] cmd = {"/bin/sh", "-c", "cd ../../Soffritti ; ./startServer.sh"};
				soff = Runtime.getRuntime().exec(cmd);
			}
			Thread.sleep(2000); //Set up soffritti
			
			//Set up local connections
			try {
				SoffrittiTCP.reinitSoffritti("localhost", 8999);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//This is for error checking
//				BufferedReader reader = new BufferedReader (new InputStreamReader (soff.getInputStream()));
//				String line;
//				while ((line = reader.readLine()) != null) {
//					System.out.println(line);
//				}
//				System.out.println("No more output");
//				BufferedReader error = new BufferedReader (new InputStreamReader(soff.getErrorStream()));
//				while ((line = error.readLine()) != null) {
//					System.out.println(line);
//				}
//				System.out.println("No more errors");
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getStackTrace().toString());
		}
	}
	
	@Test
	public void noObstacleTest() {
		try {
			String[] cmd = {"/bin/sh", "-c", "cp soffrittiScene/sceneConfigNoObstacle.js ../../Soffritti/WebGLScene/sceneConfig.js"};
			Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		chrome = new ChromeDriver();
		chrome.get("http://localhost:8081");
		chrome.manage().window().setSize(dim);
		chrome.manage().window().setPosition(pos);
		//Bring Chrome to front, otherwise Soffritti may not send collision data
		chrome.switchTo().window(chrome.getWindowHandle());
		
		//Wait for Chrome to open
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		
		Program.resetAll();
		
		Program.clean();
		
		//Room is clean
		assertTrue(RoomMap.getRoomMap().isClean());
		
		//Robot is on sonar2
		assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-2, RoomMap.getRoomMap().getDimY()-2).isRobot());
		
		//Down and Right walls have correctly detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
			assertTrue(RoomMap.getRoomMap().get(i, RoomMap.getRoomMap().getDimY()-1).isObstacle());
		}
		for (int i=0; i<RoomMap.getRoomMap().getDimY(); i++) {
			assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-1, i).isObstacle());
		}
		
		//No other obstacle has been detected and room is REALLY clean
		for (int i=0; i<RoomMap.getRoomMap().getDimX()-1; i++) {
			for (int j=0; j<RoomMap.getRoomMap().getDimY()-1; j++) {
				//Not an obstacle
				assertTrue(!RoomMap.getRoomMap().get(i, j).isObstacle());
				
				//Is clean
				assertTrue(!RoomMap.getRoomMap().get(i, j).isDirty());
			}
		}
		
	}
	
	@Test
	public void verticalObstacleTest() {
		try {
			String[] cmd = {"/bin/sh", "-c", "cp soffrittiScene/sceneConfigVerticalObstacle.js ../../Soffritti/WebGLScene/sceneConfig.js"};
			Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		chrome = new ChromeDriver();
		chrome.get("http://localhost:8081");
		chrome.manage().window().setSize(dim);
		chrome.manage().window().setPosition(pos);
		//Bring Chrome to front, otherwise Soffritti may not send collision data
		chrome.switchTo().window(chrome.getWindowHandle());
		
		//Wait for Chrome to open
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		
		Program.resetAll();
		
		Program.clean();
		
		//Room is clean
		assertTrue(RoomMap.getRoomMap().isClean());
		
		//Robot is on sonar2
		assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-2, RoomMap.getRoomMap().getDimY()-2).isRobot());
		
		//Down and Right walls have correctly detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
			assertTrue(RoomMap.getRoomMap().get(i, RoomMap.getRoomMap().getDimY()-1).isObstacle());
		}
		for (int i=0; i<RoomMap.getRoomMap().getDimY(); i++) {
			assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-1, i).isObstacle());
		}
		
		//Room is clean, vertical obstacle detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX()-1; i++) {
			for (int j=0; j<RoomMap.getRoomMap().getDimY()-1; j++) {
				
				//Vertical obstacle
				if ((i == 4) && ((j >=1) && (j <= 6))) {
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle());
				} //Still vertical obstacle, can happen because of Soffritti's timings
				else if ((i == 4) && ((j >=1) && (j <= 6))) {
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle() || !RoomMap.getRoomMap().get(i, j).isDirty());
				} else {
					//Not an obstacle
					assertTrue(!RoomMap.getRoomMap().get(i, j).isObstacle());
					
					//Is clean
					assertTrue(!RoomMap.getRoomMap().get(i, j).isDirty());
				}
			}
		}
		
	}
	
	@Test
	public void horizontalObstacleTest() {
		try {
			String[] cmd = {"/bin/sh", "-c", "cp soffrittiScene/sceneConfigHorizontalObstacle.js ../../Soffritti/WebGLScene/sceneConfig.js"};
			Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		chrome = new ChromeDriver();
		chrome.get("http://localhost:8081");
		chrome.manage().window().setSize(dim);
		chrome.manage().window().setPosition(pos);
		//Bring Chrome to front, otherwise Soffritti may not send collision data
		chrome.switchTo().window(chrome.getWindowHandle());
		
		//Wait for Chrome to open
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		
		Program.resetAll();
		
		Program.clean();
		
		//Room is clean
		assertTrue(RoomMap.getRoomMap().isClean());
		
		//Robot is on sonar2
		assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-2, RoomMap.getRoomMap().getDimY()-2).isRobot());
		
		//Down and Right walls have correctly detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
			assertTrue(RoomMap.getRoomMap().get(i, RoomMap.getRoomMap().getDimY()-1).isObstacle());
		}
		for (int i=0; i<RoomMap.getRoomMap().getDimY(); i++) {
			assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-1, i).isObstacle());
		}
		
		//Room is clean, horizontal obstacle detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX()-1; i++) {
			for (int j=0; j<RoomMap.getRoomMap().getDimY()-1; j++) {
				
				//Horizontal obstacle
				if ((j == 4) && (i >= 0 && i <= 3)) {
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle());
				} //Still horizontal obstacle, can happen because of Soffritti's timings
				else if ( ((j == 5) && ((i >= 0 && i <= 3))) || ((i==4) && (j==5 || j==4)) ){
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle() || !RoomMap.getRoomMap().get(i, j).isDirty());
				} else {
					//Not an obstacle
					assertTrue(!RoomMap.getRoomMap().get(i, j).isObstacle());
					
					//Is clean
					assertTrue(!RoomMap.getRoomMap().get(i, j).isDirty());
				}
			}
		}
		
	}
	
	@Test
	public void verticalAndHorizontalObstaclesTest() {
		try {
			String[] cmd = {"/bin/sh", "-c", "cp soffrittiScene/sceneConfig2Obstacles.js ../../Soffritti/WebGLScene/sceneConfig.js"};
			Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		chrome = new ChromeDriver();
		chrome.get("http://localhost:8081");
		chrome.manage().window().setSize(dim);
		chrome.manage().window().setPosition(pos);
		//Bring Chrome to front, otherwise Soffritti may not send collision data
		chrome.switchTo().window(chrome.getWindowHandle());
		
		//Wait for Chrome to open
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			fail(e.getStackTrace().toString());
			e.printStackTrace();
		}
		
		Program.resetAll();
		
		Program.clean();
		
		//Room is clean
		assertTrue(RoomMap.getRoomMap().isClean());
		
		//Robot is on sonar2
		assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-2, RoomMap.getRoomMap().getDimY()-2).isRobot());
		
		//Down and Right walls have correctly detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
			assertTrue(RoomMap.getRoomMap().get(i, RoomMap.getRoomMap().getDimY()-1).isObstacle());
		}
		for (int i=0; i<RoomMap.getRoomMap().getDimY(); i++) {
			assertTrue(RoomMap.getRoomMap().get(RoomMap.getRoomMap().getDimX()-1, i).isObstacle());
		}
		
		//Room is clean, vertical and horizontal obstacles detected
		for (int i=0; i<RoomMap.getRoomMap().getDimX()-1; i++) {
			for (int j=0; j<RoomMap.getRoomMap().getDimY()-1; j++) {
				
				//Horizontal obstacle
				if ((j == 4) && (i >= 0 && i <= 3)) {
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle());
				} //Still horizontal obstacle, can happen because of Soffritti's timings
				else if ( ((j == 5) && ((i >= 0 && i <= 3))) || ((i==4) && (j==5 || j==4)) ){
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle() || !RoomMap.getRoomMap().get(i, j).isDirty());
				} //Vertical obstacle
				else if ((i == 4) && ((j >=1) && (j <= 6))) {
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle());
				} //Still vertical obstacle, can happen because of Soffritti's timings
				else if ((i == 4) && ((j >=1) && (j <= 6))) {
					//IsObstacle
					assertTrue(RoomMap.getRoomMap().get(i, j).isObstacle() || !RoomMap.getRoomMap().get(i, j).isDirty());
				}
				else {
					//Not an obstacle
					assertTrue(!RoomMap.getRoomMap().get(i, j).isObstacle());
					
					//Is clean
					assertTrue(!RoomMap.getRoomMap().get(i, j).isDirty());
				}
			}
		}
		
	}
	
	@After
	public void closeChrome() {
		chrome.close();
	}
	
	@After
	public void closeSoffritti() {
		try {
			soff.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
