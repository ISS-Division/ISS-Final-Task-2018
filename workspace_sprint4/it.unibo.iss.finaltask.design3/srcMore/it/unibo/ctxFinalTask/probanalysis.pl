%====================================================================================
% Context ctxFinalTask  SYSTEM-configuration: file it.unibo.ctxFinalTask.probanalysis.pl 
%====================================================================================
context(ctxrasprobot, "192.168.43.246",  "TCP", "8571" ).  		 
context(ctxfinaltask, "localhost",  "TCP", "8570" ).  		 
%%% -------------------------------------------
qactor( guisimulator , ctxfinaltask, "it.unibo.guisimulator.MsgHandle_Guisimulator"   ). %%store msgs 
qactor( guisimulator_ctrl , ctxfinaltask, "it.unibo.guisimulator.Guisimulator"   ). %%control-driven 
qactor( usercmdinterpreter , ctxfinaltask, "it.unibo.usercmdinterpreter.MsgHandle_Usercmdinterpreter"   ). %%store msgs 
qactor( usercmdinterpreter_ctrl , ctxfinaltask, "it.unibo.usercmdinterpreter.Usercmdinterpreter"   ). %%control-driven 
qactor( temperatureprovider , ctxfinaltask, "it.unibo.temperatureprovider.MsgHandle_Temperatureprovider"   ). %%store msgs 
qactor( temperatureprovider_ctrl , ctxfinaltask, "it.unibo.temperatureprovider.Temperatureprovider"   ). %%control-driven 
qactor( timeprovider , ctxfinaltask, "it.unibo.timeprovider.MsgHandle_Timeprovider"   ). %%store msgs 
qactor( timeprovider_ctrl , ctxfinaltask, "it.unibo.timeprovider.Timeprovider"   ). %%control-driven 
qactor( ledcontrol , ctxfinaltask, "it.unibo.ledcontrol.MsgHandle_Ledcontrol"   ). %%store msgs 
qactor( ledcontrol_ctrl , ctxfinaltask, "it.unibo.ledcontrol.Ledcontrol"   ). %%control-driven 
qactor( robotcontrol , ctxfinaltask, "it.unibo.robotcontrol.MsgHandle_Robotcontrol"   ). %%store msgs 
qactor( robotcontrol_ctrl , ctxfinaltask, "it.unibo.robotcontrol.Robotcontrol"   ). %%control-driven 
qactor( basesoffrittirobot , ctxfinaltask, "it.unibo.basesoffrittirobot.MsgHandle_Basesoffrittirobot"   ). %%store msgs 
qactor( basesoffrittirobot_ctrl , ctxfinaltask, "it.unibo.basesoffrittirobot.Basesoffrittirobot"   ). %%control-driven 
qactor( testsonar , ctxfinaltask, "it.unibo.testsonar.MsgHandle_Testsonar"   ). %%store msgs 
qactor( testsonar_ctrl , ctxfinaltask, "it.unibo.testsonar.Testsonar"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(collisionevh,ctxfinaltask,"it.unibo.ctxFinalTask.Collisionevh","collisionEvent").  
eventhandler(sonarevh,ctxfinaltask,"it.unibo.ctxFinalTask.Sonarevh","sonarEvent").  
%%% -------------------------------------------

