package it.unibo.exploremap.program;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import aima.core.agent.Action;
import aima.core.search.framework.SearchAgent;
import aima.core.search.framework.problem.GoalTest;
import aima.core.search.framework.problem.Problem;
import aima.core.search.framework.qsearch.GraphSearch;
import aima.core.search.uninformed.BreadthFirstSearch;
import it.unibo.exploremap.model.Box;
import it.unibo.exploremap.model.Functions;
import it.unibo.exploremap.model.RobotAction;
import it.unibo.exploremap.model.RobotState;
import it.unibo.exploremap.model.RobotState.Direction;
import it.unibo.exploremap.model.RoomMap;
import it.unibo.qactors.akka.QActor;
import it.unibo.utils.soffrittiTCP;

public class ai {
	
	public static final int MOVETIME = 300;
	public static final int TURNTIME = 1000;
	private static final String POST_URL = "http://localhost:3000/robot/actions/commands/stop";
	
	private static boolean wallDownFound = false;
	private static boolean wallRightFound = false;
	private static RobotState initialState;
	
	private static boolean goToSonar1 = false;
	
	private static QActor actor;
	
	private static Thread aiThread;
	private static ai program;
	
	public static void initAI(QActor qa) throws Exception {
		actor = qa;
		program = new ai();
		soffrittiTCP.initConnQa();
		initialState = new RobotState(0, 0, RobotState.Direction.DOWN);
	}
	
	public static void cleanQa(QActor qa) throws UnknownHostException, IOException {
		RoomMap.getRoomMap().setDirty();
		//Flush Buffers
//		try {
//			//Flush input
//			while(true) {
//				soffrittiTCP.getSoffrittiSonars().readLine();
//			}
//		} catch (SocketTimeoutException e) {
//		}
		try {
			//Flush input
			while(true) {
				soffrittiTCP.getSoffrittiCollisions().readLine();
			}
		} catch (SocketTimeoutException e) {
		}
		try {
			//Flush input
			while(true) {
				soffrittiTCP.getStops().readLine();
			}
		} catch (SocketTimeoutException e) {
		}

		aiThread = new Thread() {
			@Override
			public void run() {
				program.clean();
			}
		};
		aiThread.start();
	}
	
	public static void main(String[] args) {
		ai p = new ai();
		try {
			soffrittiTCP.initConnTest("localhost", 8999);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		initialState = new RobotState(0, 0, RobotState.Direction.DOWN);
		p.clean();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Going back to sonar1");
		p.goToSonar1();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Cleaning again");
		RoomMap.getRoomMap().setDirty();
		p.clean();
		try {
			if (soffrittiTCP.soffrittiListeningThread != null)
				soffrittiTCP.soffrittiListeningThread.stop();
			soffrittiTCP.localCollisionSocket.close();
			soffrittiTCP.localSendCollisionSocket.close();
			soffrittiTCP.localSonarSocket.close();
			soffrittiTCP.localSendSonarSocket.close();
			soffrittiTCP.localServerSocket.close();
			soffrittiTCP.clientSocket.close();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void goToSonar1() {
		goToSonar1 = true;
		clean();
		goToSonar1 = false;
	}
	
	@SuppressWarnings("deprecation")
	public void clean() {
		
		System.out.println(RoomMap.getRoomMap().toString());
		
		BreadthFirstSearch search = new BreadthFirstSearch(new GraphSearch());
//		DepthFirstSearch search = new DepthFirstSearch(new GraphSearch());
		SearchAgent searchAgent;
		for (int k=0; k<Integer.MAX_VALUE; k++) {
			try {
				List<Action> actions;
				GoalTest goalTest;
				if (!goToSonar1) {
					if (RoomMap.getRoomMap().isClean() && wallDownFound && wallRightFound) {
						if (initialState.getX() == RoomMap.getRoomMap().getDimX()-2 &&
								initialState.getY() == RoomMap.getRoomMap().getDimY()-2) {
							System.out.println("FINISHED");
							break;
						}
						System.out.println("Finished cleaning, going to sonar2");
						goalTest = new GoalTest() {
							
							@Override
							public boolean isGoalState(Object state) {
								RobotState robotState = (RobotState) state;
								if (robotState.getX() == RoomMap.getRoomMap().getDimX()-2 &&
										robotState.getY() == RoomMap.getRoomMap().getDimY()-2 && robotState.getDirection() == Direction.RIGHT)
									return true;
								else
									return false;
							}
						};
					} else
						goalTest = new Functions();
					Problem problem = new Problem(initialState, new Functions(), new Functions(), goalTest, new Functions());
					if (wallDownFound && !wallRightFound) {
						actions = new ArrayList<>();
						if (initialState.getDirection() == Direction.DOWN)
							actions.add(new RobotAction(RobotAction.TURNLEFT));
						actions.add(new RobotAction(RobotAction.FORWARD));
					} else {
						searchAgent = new SearchAgent(problem, search);
						actions = searchAgent.getActions();
						if (actions == null || actions.isEmpty()) {
							if (!RoomMap.getRoomMap().isClean())
								RoomMap.getRoomMap().setObstacles();
							actions = new ArrayList<Action>();
						}
					}
					if ((actions == null || actions.isEmpty()) && (!wallDownFound || !wallRightFound))
						break;
				} else {
					Problem problem = new Problem(initialState, new Functions(), new Functions(), new GoalTest() {

						@Override
						public boolean isGoalState(Object state) {
							RobotState robotState = (RobotState) state;
							if (robotState.getX() == 0 &&
									robotState.getY() == 0 && robotState.getDirection() == Direction.DOWN)
								return true;
							else
								return false;
						}
						
					}, new Functions());
					searchAgent = new SearchAgent(problem, search);
					actions = searchAgent.getActions();
					if (actions == null || actions.isEmpty() || actions.get(0).isNoOp())
						break;
				}
				System.out.println("Step " + k + "\n" + actions);
				for (Action ac : actions) {
					RobotAction a = (RobotAction) ac;
//					try {
//						//Flush input
//						while(true) {
//							System.out.println("Main:" + SoffrittiTCP.getSoffrittiSonars().readLine());
//						}
//					} catch (SocketTimeoutException e) {
//						System.out.println("Sonar buffer is now empty");
//					}
					if (soffrittiTCP.localStopSocket != null) {
						boolean stop = false;
						try {
							soffrittiTCP.getStops().readLine();
							//Stop cmd arrived
							stop = true;
						} catch (SocketTimeoutException e) {
							
						}
						if (stop)
							return; //STOPP						
					}
					moveRobot(a);
					if (a.getAction() == RobotAction.FORWARD) {
						long time = System.currentTimeMillis();
						String input;
						try {
							//Timing erros, this is needed to synchronize the position on the 4 sides
							if (initialState.getX() == 1 && initialState.getDirection() == Direction.LEFT ||
								initialState.getY() == 1 && initialState.getDirection() == Direction.UP ||
								(wallDownFound && initialState.getY() == RoomMap.getRoomMap().getDimY()-3 && initialState.getDirection() == Direction.DOWN) ||
								(wallRightFound && initialState.getX() == RoomMap.getRoomMap().getDimX()-3 && initialState.getDirection() == Direction.RIGHT) ) {
								System.out.println("Position synchronized with wall");
								Thread.sleep(MOVETIME + 50);
								moveRobot(a); //Needed for bad timings
								Thread.sleep(MOVETIME + 50);
								try {
									//Flush input
									while(true) {
										soffrittiTCP.getSoffrittiCollisions().readLine();
									}
								} catch (SocketTimeoutException e) {
									
								}
								RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, false));
								initialState = (RobotState) new Functions().result(initialState, a);
								RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, true)); 
								continue;
							}
							input = soffrittiTCP.getSoffrittiCollisions().readLine();
							time = System.currentTimeMillis() - time;
							System.out.println("Collision detected");
							moveRobot(new RobotAction(RobotAction.BACKWARD), (int)time);
							Thread.sleep(MOVETIME);
							Thread.sleep(2000); //Mobile obstacle?
							moveRobot(new RobotAction(RobotAction.FORWARD), MOVETIME);
							time = System.currentTimeMillis();
							input = soffrittiTCP.getSoffrittiCollisions().readLine();
							time = System.currentTimeMillis() - time;
							RobotState temp = (RobotState) new Functions().result(initialState, a);
							RoomMap.getRoomMap().put(temp.getX(), temp.getY(), new Box(true, false));
							System.out.println("Fixed obstacle, new plan...");
							moveRobot(new RobotAction(RobotAction.BACKWARD), (int)time);
							Thread.sleep(time);
							
							//If there is a sonar event from sonar2
							if (!wallDownFound && initialState.getDirection() == Direction.DOWN) {
								Thread.sleep(1000); //Wait for the sonar event to arrive
								try {
									String sonarEv = soffrittiTCP.getSoffrittiSonars().readLine();
									System.out.println("WallDown: " + sonarEv);
									while (!sonarEv.contains("sonar2")) {
										sonarEv = soffrittiTCP.getSoffrittiSonars().readLine();
										System.out.println("WallDown: " + sonarEv);
									}
									
									//Sonar2 detected, update map
									for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
										RoomMap.getRoomMap().put(i, initialState.getY()+1, new Box(true, false, false));
									}
									wallDownFound = true;
									//Map changed, replan
									System.out.println("WallDown found, replan...");
									break;
								} catch (SocketTimeoutException e) {
									//No sonar2, go ahead
									System.out.println("No sonar2");
								}
							}
							else if (wallDownFound && !wallRightFound && initialState.getDirection() == Direction.RIGHT) {
								for (int i=0; i<RoomMap.getRoomMap().getDimY(); i++) {
									RoomMap.getRoomMap().put(initialState.getX()+1, i, new Box(true, false, false));
								}
								wallRightFound = true;
								//Map changed, replan
								System.out.println("WallRight found, replan...");
								break;
							}
							
							Thread.sleep(MOVETIME);
							break;
						} catch (SocketTimeoutException e) {
							System.out.println("No collision");
							RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, false));
//							System.out.println(RoomMap.getRoomMap().toString());
							initialState = (RobotState) new Functions().result(initialState, a);
							RoomMap.getRoomMap().put(initialState.getX(), initialState.getY(), new Box(false, false, true));
							
							//If there is a sonar event from sonar2
							if (wallDownFound && !wallRightFound && initialState.getDirection() == Direction.RIGHT) {
								for (int i=0; i<RoomMap.getRoomMap().getDimX(); i++) {
									RoomMap.getRoomMap().put(i, initialState.getY()+1, new Box(true, false, false));
								}
								wallDownFound = true;
								//Map changed, replan
								System.out.println("WallDown found, replan...");
								break;
							}
							
//							Thread.sleep(500);
						}
						
					} else {
						initialState = (RobotState) new Functions().result(initialState, a);
						Thread.sleep(TURNTIME);
					}
						
				}
				System.out.println(RoomMap.getRoomMap().toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(RoomMap.getRoomMap().toString());
		System.out.println("Algorithm end");
		if (actor != null)
			try {
//				actor.emit("usercmd", "usercmd(robotgui(stopp))");
				//POST to NODE
				URL obj = new URL(POST_URL);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				int responseCode = con.getResponseCode();
//				OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
//				out.write("	{\"start\":true}");
				System.out.println("PUT Response Code :: " + responseCode);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public static void moveRobot(RobotAction a) throws Exception {
		switch(a.getAction()) {
		case RobotAction.FORWARD: moveRobot(a, MOVETIME); break;
		case RobotAction.BACKWARD: moveRobot(a, MOVETIME); break;
		case RobotAction.TURNLEFT: moveRobot(a, TURNTIME); break;
		case RobotAction.TURNRIGHT: moveRobot(a, TURNTIME); break;
		default: System.out.println("Error, action unrecognized");
		}
	}
	
	public static void moveRobot(RobotAction a, int time) throws Exception {
		if (actor == null) {
			switch(a.getAction()) {
			case RobotAction.FORWARD: soffrittiTCP.soffrittiCommand("moveForward", ""+time); break;
			case RobotAction.BACKWARD: soffrittiTCP.soffrittiCommand("moveBackward", ""+time); break;
			case RobotAction.TURNLEFT: soffrittiTCP.soffrittiCommand("turnLeft", ""+time); break;
			case RobotAction.TURNRIGHT: soffrittiTCP.soffrittiCommand("turnRight", ""+time); break;
			default: System.out.println("Error, action unrecognized");
			}
		} else {
			if (actor.solveGoal("soffrittiConnected(true)").isSuccess()) {
				switch(a.getAction()) {
				case RobotAction.FORWARD: actor.sendMsg("robotCmd", "basesoffrittirobot", "dispatch", "robotCmd(w,"+time+")"); break;
				case RobotAction.BACKWARD: actor.sendMsg("robotCmd", "basesoffrittirobot", "dispatch", "robotCmd(s,"+time+")"); break;
				case RobotAction.TURNLEFT: actor.sendMsg("robotCmd", "basesoffrittirobot", "dispatch", "robotCmd(a,"+time+")"); break;
				case RobotAction.TURNRIGHT: actor.sendMsg("robotCmd", "basesoffrittirobot", "dispatch", "robotCmd(d,"+time+")"); break;
				default: System.out.println("Error, action unrecognized");
				}
			}
			if (actor.solveGoal("realConnected(true)").isSuccess()) {
				switch(a.getAction()) {
				case RobotAction.FORWARD: actor.sendExtMsg("robotCmd", "realrobot", "ctxRaspRobot", "dispatch", "robotCmd(w,"+time+")"); break;
				case RobotAction.BACKWARD: actor.sendExtMsg("robotCmd", "realrobot", "ctxRaspRobot", "dispatch", "robotCmd(s,"+time+")"); break;
				case RobotAction.TURNLEFT: actor.sendExtMsg("robotCmd", "realrobot", "ctxRaspRobot", "dispatch", "robotCmd(a,"+time+")"); break;
				case RobotAction.TURNRIGHT: actor.sendExtMsg("robotCmd", "realrobot", "ctxRaspRobot", "dispatch", "robotCmd(d,"+time+")"); break;
				default: System.out.println("Error, action unrecognized");
				}
			}
		}
	}
}
