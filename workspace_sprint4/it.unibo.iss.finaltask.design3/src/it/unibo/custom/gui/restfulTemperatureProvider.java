package it.unibo.custom.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class restfulTemperatureProvider {
	
	private static final String GET_URL = "http://api.openweathermap.org/data/2.5/weather?q=Bologna&appid=6d3698b4dab688aed8e4f9466910fdf7";
	private static restfulTemperatureProvider currentRestfulTemperatureProvider = null;
	private static double temp;
	private static QActor actor;
	
	
	public restfulTemperatureProvider(QActor myActor) {
    	actor = myActor;
	}
	
	public static synchronized restfulTemperatureProvider createRestfulTemperatureProvider(QActor myActor) { //Factory method
    	if (currentRestfulTemperatureProvider == null ) currentRestfulTemperatureProvider = new restfulTemperatureProvider(myActor);
    	threadUpdate(myActor);
    	return currentRestfulTemperatureProvider;
    }
	
	public static void sendMsgMqtt(  String topic, String msgID, String dest, String msg ) throws Exception{
// 		println("			%%%  sendMsgMqtt "  + dest + " msgID=" + msgID + " msg=" + msg);
		String msgToPublish="";
		if( dest.equals("none")){ 
			msgToPublish = QActorUtils.buildEventItem(actor.getName(), msgID, msg).getPrologRep();
 		}else{
 			msgToPublish = QActorUtils.buildMsg(actor.getQActorContext(), actor.getName(), msgID, dest, "dispatch", msg).getDefaultRep();
 		}
 		actor.publish( actor.getName(),  topic, msgToPublish, 1, true);  //1 is qos
	} 
	
	public static void sendTemperature(QActor myActor) throws Exception {
		double value = sendGET();
		myActor.emit("temperatureEvent", "temperatureEvent("+value+")");
		sendMsgMqtt("unibo/qasys", "temperatureEvent", "none", "temperatureEvent("+value+")");
	}
	
	public static double sendGET() throws IOException {
		URL obj = new URL(GET_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			
			String inpuStr = in.readLine();
			
			// print result
			System.out.println(inpuStr);
			
			JSONObject jsonObject = new JSONObject(inpuStr);
			JSONObject temperature = jsonObject.getJSONObject("main");
			System.out.println(temperature.toString());
			
			temp = temperature.getDouble("temp");
			
			temp = temp- 273.15;
			round(temp,2);
			
			System.out.println(temp);
			
			
			
		} else {
			System.out.println("GET request not worked");
		}
		return temp;
	}
	
//	public static void main(String[] args) throws Exception {
//		sendGET();
//	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	public static void threadUpdate(QActor qa) {		
		new Thread() {
			public void run() {
				while( true ) {	
					try {
						sleep(5000);
						sendTemperature(qa);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
}



