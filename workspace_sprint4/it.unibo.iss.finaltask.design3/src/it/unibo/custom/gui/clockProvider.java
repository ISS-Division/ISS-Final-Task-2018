package it.unibo.custom.gui;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import alice.tuprolog.Int;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class clockProvider {
	
	private static clockProvider currentclockProvider = null;
	private static QActor actor;
	
	public clockProvider(QActor myActor) {
    	actor = myActor;
    	ArrayList<Int> list = new ArrayList<>();
    	
	}
	
	public static synchronized clockProvider createClockProvider(QActor myActor) { //Factory method
    	if (currentclockProvider == null ) currentclockProvider = new clockProvider(myActor);
    	threadUpdate(myActor);
    	return currentclockProvider;
    }
	
	public static void sendMsgMqtt(  String topic, String msgID, String dest, String msg ) throws Exception{
// 		println("			%%%  sendMsgMqtt "  + dest + " msgID=" + msgID + " msg=" + msg);
		String msgToPublish="";
		if( dest.equals("none")){ 
			msgToPublish = QActorUtils.buildEventItem(actor.getName(), msgID, msg).getPrologRep();
 		}else{
 			msgToPublish = QActorUtils.buildMsg(actor.getQActorContext(), actor.getName(), msgID, dest, "dispatch", msg).getDefaultRep();
 		}
 		actor.publish( actor.getName(),  topic, msgToPublish, 1, true);  //1 is qos
	} 
	
	public static void sendTime(QActor myActor) throws Exception {
		ArrayList<Integer>  time = getClock();
		int hours = time.get(0);
		int mins = time.get(1);
		myActor.emit("timeEvent", "timeEvent("+hours+", "+mins+")");
		sendMsgMqtt("unibo/qasys", "timeEvent", "none", "timeEvent("+hours+", "+mins+")");
	}
	
	public static ArrayList<Integer>  getClock() throws IOException {
		LocalDateTime time = LocalDateTime.now();
		ArrayList<Integer> result = new ArrayList<>();
		System.out.println(time.getHour());
		System.out.println(time.getMinute());
		result.add(time.getHour());
		result.add(time.getMinute());
		return result;
	}
	
	public static void threadUpdate(QActor qa) {		
		new Thread() {
			public void run() {
				while( true ) {	
					try {
						sleep(5000);
						sendTime(qa);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
}
