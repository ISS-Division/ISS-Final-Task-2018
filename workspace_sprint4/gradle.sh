#!/bin/bash

for D in `find . -regex '^\./w.*' -maxdepth 1 -mindepth 1 -type d`
do
	( cd $D && ./gradle.sh && cd ..)
done
