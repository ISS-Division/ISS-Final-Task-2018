/*
 * frontend/plugins/internal/modelManagerPlugin.js
 */
var resourceModel = require('./../../appServer/models/model');
var observable    = require('./../../uniboSupports/observableFactory'); 	 
var mqttUtils     = require('./../../uniboSupports/mqttUtils'); 	 

var actuator, interval;
var ledModels     		= resourceModel.leds;
var robotModels	  		= resourceModel.robots;
var temperatureModel	= resourceModel.temperature;
var pluginName    		= "Model Manager";
var localParams   		= {'simulate': false, 'frequency': 2000};
var counter       		= 1;

exports.start = function (params) {
  localParams = params;
  for (var robot in robotModels)
	  if (robotModels[robot].connected)
		  observeRobots(robotModels[robot]);
//  for (var led in ledModels)
//	  observe(ledModels[led]);
//  observe(temperatureModel);

  if (localParams.simulate) {
//    simulate();		 
  } else {
    // connectHardware();
  }
};

exports.stop = function () {
  if (localParams.simulate) {
    clearInterval(interval);
  } else {
    actuator.unexport();
  }
  console.info('%s plugin stopped!', pluginName);
};

function observeRobots(what) {
	console.log('plugin observe: ' + localParams.frequency + " CHANGE MODEL INTO OBSERVABLE");
 	console.log( what ); 
 	//Change the robotModel into an observable;	
 	const whatObservable = observable(what);	
 	observableObj = whatObservable.data;
 	whatObservable.observe('state', () => {
		var val =  "stopp";
		if( observableObj.state === "cleaning" ) val = "start";
		console.log(observableObj.name + " " + observableObj.state);
		var eventstr = "msg(usercmd,event,js,none,usercmd(robotgui(" +val + "))," +  counter++ + ")"
			console.log("	ModelManagerPlugin ROBOT emits> "+ eventstr);
	 		mqttUtils.publish( eventstr );
  	});
}

function observe(what) {
	console.info('plugin observe: ' + localParams.frequency + " CHANGE MODEL INTO OBSERVABLE");
 	console.info( what ); 
//Change the ledModel into an observable;	
const whatObservable = observable(what);	
observableObj = whatObservable.data;
whatObservable.observe('state', () => {
	var val =  "off";
	if( observableObj.value === "true" ) val = "on";
	var eventstr = "msg(ctrlEvent,event,js,none,ctrlEvent(leds, led1, " +val + ")," +  counter++ + ")"
		console.log("	ledPlugin LED emits> "+ eventstr);
// 		mqttUtils.publish( eventstr );
  	});
};

function switchOnOff(value) {
  if (!localParams.simulate) {
    actuator.write(value === true ? 1 : 0, function () {  
      console.info('Changed value of %s to %s', pluginName, value);
    });
  }
};

function connectHardware() {
  var Gpio = require('onoff').Gpio;
  actuator = new Gpio(ledModel.gpio, 'out');  
  console.info('Hardware %s actuator started!', pluginName);
};

function simulate() {
  interval = setInterval(function () {
    // Switch value on a regular basis;
    if (ledModel.value) {
      ledModel.value = false;
    } else {
      ledModel.value = true;
    }
//  console.log("LED=" + ledModel.value);
  }, localParams.frequency);
  console.info('Simulated %s actuator started!', pluginName);
};