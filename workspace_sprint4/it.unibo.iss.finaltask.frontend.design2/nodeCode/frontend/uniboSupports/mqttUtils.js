/*
* =====================================
* uniboSupports/mqttUtils.js
* =====================================
*/
const mqtt   = require ('mqtt');
var resourceModel   = require('../appServer/models/model');
var temperatureModel = resourceModel.temperature;
var robots = resourceModel.robots;
var leds = resourceModel.leds;
var timeModel = resourceModel.time;
const topic  = "unibo/qasys";
//var client = mqtt.connect('mqtt://iot.eclipse.org');
var client   = mqtt.connect('mqtt://localhost');
//var client   = mqtt.connect('tcp://192.168.43.229:1883');

console.log("mqtt client= " + client );

exports.publish = function( msg ){
	//console.log('mqtt publish ' + client);
	client.publish(topic, msg);
}

client.on('connect', function () {
	  client.subscribe( topic );
	  console.log('client has subscribed successfully ');
});

//The message usually arrives as buffer, so I had to convert it to string data type;
client.on('message', function (topic, message){
  console.log("mqtt RECEIVES:"+ message.toString()); //if toString is not given, the message comes as buffer
  var msgId = message.toString().substring(4, message.toString().indexOf(","));
  console.log("msgId: "+msgId);
  if (msgId === "temperatureEvent") {
	  var tempMsg = message.toString().substring(message.toString().lastIndexOf("temperatureEvent")+
			  "temperatureEvent".length+1, message.toString().length);
	  console.log("tempMsg:"+tempMsg);
	  var temperatureString = tempMsg.substring(0, tempMsg.indexOf(")"));
	  console.log("temperatureString:"+temperatureString);
	  var temperature = parseInt(temperatureString);
	  console.log("temperature:"+temperature);
	  //CONDITION CHECK [R-Temp/R-Time]
	  if (temperatureModel.currentTemperature <= 25 && temperature > 25) {
		  var oneOn = false;
		  for (var robot in robots) {
				if (robots[robot].connected) {
					if (robots[robot].state === "on")
						oneOn = true;
					robots[robot].state = "off";
				}
			}
			for (var led in leds) {
				if (robots[led].connected) {
					leds[led].state = "off";
				}
			}
		if (oneOn == true) {
		  var eventstr = "msg(usercmd,event,js,none,usercmd(robotgui(stopp, "+robots[0].connected+", "+robots[1].connected+")),1)"
			console.log("emits> "+ eventstr);
		  client.publish(topic, eventstr );
	  	}
	  }
	  temperatureModel.currentTemperature = temperature; //Model update
  } else if (msgId === "timeEvent") {
	  var tempMsg = message.toString().substring(message.toString().lastIndexOf("timeEvent")+
			  "timeEvent".length+1, message.toString().length);
	  console.log("tempMsg:"+tempMsg);
	  var hoursString = tempMsg.substring(0, tempMsg.indexOf(")"));
	  tempMsg = tempMsg.substring(tempMsg.indexOf(",")+1, tempMsg.length);
	  var minutesString = tempMsg.substring(0, tempMsg.indexOf(")"));
	  var hours = parseInt(hoursString);
	  var minutes = parseInt(minutesString);
	  console.log("hours:"+hours + ", minutes:"+minutes);
	  if ( ((timeModel.hours >= 7 && timeModel.hours <= 9) || (timeModel.hours == 10 && timeModel.minutes == 0)) && 
			  (hours < 7 || hours > 10 ||(hours==10 && minutes > 0)) ) {
		  var oneOn = false;
		  for (var robot in robots) {
				if (robots[robot].connected) {
					if (robots[robot].state === "on")
						oneOn = true;
				}
		  }
			for (var led in leds) {
				if (robots[led].connected) {
					leds[led].state = "off";
				}
			}
		if (oneOn == true) {
		  var eventstr = "msg(usercmd,event,js,none,usercmd(robotgui(stopp, "+robots[0].connected+", "+robots[1].connected+")),1)"
			console.log("emits> "+ eventstr);
		  client.publish(topic, eventstr );
		}
	  }
	  timeModel.hours = hours;
	  timeModel.minutes = minutes; //Model update
  }
});