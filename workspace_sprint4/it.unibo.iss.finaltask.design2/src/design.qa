System probanalysis

Dispatch credentials : credentials(NAME, PSW) //Credentials from the user
Event usercmd : usercmd(X) //[X=start|stopp] From integrated web interface
Dispatch robotCmd : robotCmd(X, TIME) //Commands to the robot
Dispatch cmd : cmd(X) //Internal system commands

Event temperatureEvent : temperatureEvent(TEMP) //Used for the communication w/ the temperature provider
Event timeEvent : timeEvent(HOURS, MINUTES) //Used for the communication w/ the time provider

//Events and dispatches from Soffritti robot
Event sonarEvent : sonar(SONAR, DISTANCE)
Dispatch sonarDispatch : sonar(SONAR, DISTANCE)
Event collisionEvent : collision(TARGET)
Dispatch collisionDispatch : collision(TARGET)

Dispatch serverDispatch : serverDispatch(X, SOFFRITTI, REAL) //[X=start|stopp, SOFFRITTI=true|false, REAL=true|false] Soffritti and Real tell if the two robots are connected or not respectively.
Dispatch serverDispatchLed : serverDispatchLed(X, HUE, LED) //[X=start|stopp, HUE=true|false, LED=true|false] Hue and Led tell if the two leds are connected or not respectively.

Dispatch localCmd : localCmd(X) //Used for self messages

pubSubServer "tcp://localhost:1883"

Context ctxFinalTask ip [ host="localhost" port=8570 ] -httpserver
//Sonar events and collisions are crucially important, they should not be lost
//The robotcontrol doesn't handle these for now, this will be analyzed in the future
//as it concerns Obstacle Avoidance requirements.
EventHandler collisionevh for collisionEvent -print {
	forwardEvent robotcontrol -m collisionDispatch
};
EventHandler sonarevh for sonarEvent {
	forwardEvent robotcontrol -m sonarDispatch
};

//[R-Start/R-Stop] Test
QActor guisimulator context ctxFinalTask {
	Plan init normal [
		println("guisimulator init")
	] switchTo emitCmd
	
	Plan emitCmd [
		delay 2000;
		
//		//Test: unknown command
//		println("guisimulator sending unknown command");
//		emit usercmd : usercmd(unknown);
//		delay 1000;
//		
//		//Test: start
//		println("guisimulator sending start");
//		emit usercmd : usercmd(robotgui(start));
//		delay 1000;
//		
//		//Test: unknown command
//		println("guisimulator sending unknown command");
//		emit usercmd : usercmd(unknown);
//		delay 1000;
//		
//		//Test: stopp
//		println("guisimulator sending stopp");
//		emit usercmd : usercmd(robotgui(stopp));
//		delay 3000;
//		
//		//Test: start again
//		println("guisimulator sending start");
//		emit usercmd : usercmd(robotgui(start));
		delay 1000
	]
}

//This is needed considering the requirements of having both a real and a virtual robot.
//This actor provides behavioral bindings between a robot and its led.
//[R-Start/R-Stop]
QActor usercmdinterpreter context ctxFinalTask -pubsub {
	
	Plan init normal [
		println("usercmdinterpreter init")
	] switchTo waitForEvents
	
	Plan waitForEvents [
		println("usercmdinterpreter: waiting for events")
	] transition stopAfter 3600000
		whenEvent usercmd -> handleEvent
	finally repeatPlan
	
	Plan handleEvent resumeLastPlan [
		onEvent usercmd : usercmd(robotgui(start, X, Y)) -> forward ledcontrol -m serverDispatchLed : serverDispatchLed(start, X, Y);
		onEvent usercmd : usercmd(robotgui(start, X, Y)) -> forward robotcontrol -m serverDispatch : serverDispatch(start, X, Y);
		onEvent usercmd : usercmd(robotgui(stopp, X, Y)) -> forward ledcontrol -m serverDispatchLed : serverDispatchLed(stopp, X, Y);
		onEvent usercmd : usercmd(robotgui(stopp, X, Y)) -> forward robotcontrol -m serverDispatch : serverDispatch(stopp, X, Y)
	]
}

//[R-TempOk/R-TempKo]
//This actor provides temperature when requested. It is up to the designer to eventually implement
//a push system. It can also act as broker for different real providers.
//Moreover, this actor can both query an external service for temperature retrieval and store it for
//reuse in case the temperature service is temporarily offline.
QActor temperatureprovider context ctxFinalTask {
	Plan init normal [
		println("temperature provider init");
		delay 1000;
		//Provider init, WORKS WITH MQTT
		connectAsPublisher "unibo/qasys";
//		javaRun it.unibo.custom.gui.temperatureMockGui.createCustomTemperatureGui(); //Mock
		javaRun it.unibo.custom.gui.restfulTemperatureProvider.createRestfulTemperatureProvider(); //Real city temperature
		println("temeperature provider initialized")
//		publishEvent "unibo/qasys" -e temperatureEvent : temperatureEvent(3)
	] transition stopAfter 3600000
	//According to the latest considerations, this actor should provide temperature at his own rate,
	//as synchronous interactions can become really bad for the end user.
}

//[R-TimeOk/R-TimeKo]
//This actor provides time when requested. It is up to the designer to eventually implement
//a push system. It can also act as broker for different real providers.
//Moreover, this actor can both query an external clock service (and store it for
//reuse in case the clock service is temporarily offline).
QActor timeprovider context ctxFinalTask {
	Plan init normal [
		println("time provider init");
		delay 1000;
		//Provider init, WORKS WITH MQTT
		connectAsPublisher "unibo/qasys";
//		javaRun it.unibo.custom.gui.timeMockGui.createCustomTimeMockGui(); //Mock
		javaRun it.unibo.custom.gui.clockProvider.createClockProvider(); //Real Clock
		println("time provider initialized")
	] transition stopAfter 3600000
}

//[R-BlinkLed/R-BlinkHue]
//This actor is now controlling both the led mock and the hue mock for demo purposes.
//Of course in the future this should be done differently.
QActor ledcontrol context ctxFinalTask { 
	Plan init normal [         
   		println("ledcontrol init");
   		javaRun it.unibo.custom.gui.ledMockGui.createCustomLedGui();
   		javaRun it.unibo.custom.gui.hueMockGui.createCustomHueGui()
   	]
   	switchTo waitForCommand     
  
    Plan waitForCommand[]  
    transition stopAfter 3600000 
     	//whenMsg cmd -> startPlan
     	whenMsg serverDispatchLed -> startPlan
    finally repeatPlan	
   	 
	Plan startPlan resumeLastPlan[
		println("led control: startplan");
		//onMsg cmd : cmd(start) -> selfMsg localCmd : localCmd(start)
		onMsg serverDispatchLed : serverDispatchLed(start, X, Y) -> ReplaceRule hueConnected(_) with hueConnected(X);
		onMsg serverDispatchLed : serverDispatchLed(start, X, Y) -> ReplaceRule ledConnected(_) with ledConnected(Y);
		onMsg serverDispatchLed : serverDispatchLed(start, _, _) -> selfMsg localCmd : localCmd(start)
	]
	transition
		whenTime 100 -> waitForCommand
		whenMsg localCmd -> blinkOn
		
	Plan blinkOn [
		println("led on");
		[ !? ledConnected(true) ] javaRun it.unibo.custom.gui.ledMockGui.setLed("on");
		[ !? hueConnected(true) ] javaRun it.unibo.custom.gui.hueMockGui.setHue("on")
	]
	transition 
		whenTime 500 -> blinkOff
		//whenMsg cmd -> commandArrived
		whenMsg serverDispatchLed -> commandArrived
	finally repeatPlan
	
	Plan blinkOff [
		println("led off");
		[ !? ledConnected(true) ] javaRun it.unibo.custom.gui.ledMockGui.setLed("off");
		[ !? hueConnected(true) ] javaRun it.unibo.custom.gui.hueMockGui.setHue("off")
	]
	transition
		whenTime 500 -> blinkOn
		//whenMsg cmd -> commandArrived
		whenMsg serverDispatchLed -> commandArrived
	finally repeatPlan
	
	Plan commandArrived resumeLastPlan [
		println("command arrived while blinking");
		//onMsg cmd : cmd(stopp) -> selfMsg localCmd : localCmd(stopp)
		onMsg serverDispatchLed : serverDispatchLed(stopp, _, _) -> selfMsg localCmd : localCmd(stopp)
	]
	transition
		whenTime 100 -> blinkOff //Should not happen
		whenMsg localCmd -> stopPlan
	
	Plan stopPlan [ 
		println("led off");
		[ !? ledConnected(true) ] javaRun it.unibo.custom.gui.ledMockGui.setLed("off");
		[ !? hueConnected(true) ] javaRun it.unibo.custom.gui.hueMockGui.setHue("off");
		println("led: blinking stopped")
	]
	switchTo waitForCommand
}


//[R-FloorClean]
//This QActor shows, just like ledcontrol, shows how to be reactive to commands
//while doing some work, as this is required.
QActor robotcontrol context ctxFinalTask {
	Plan init normal [
		println("robotcontrol init")
	] switchTo waitForCommands
	
	Plan waitForCommands [
		println("robotcontrol: waiting for commands")
	] 
	transition stopAfter 3600000
		//whenMsg cmd -> startPlan
		whenMsg serverDispatch -> startPlan 
	finally repeatPlan
		
	Plan startPlan [
		println("command arrived");
		//onMsg cmd : cmd(start) -> selfMsg localCmd : localCmd(start)
		onMsg serverDispatch : serverDispatch(start, X, Y) -> ReplaceRule soffrittiConnected(_) with soffrittiConnected(X);
		onMsg serverDispatch : serverDispatch(start, X, Y) -> ReplaceRule realConnected(_) with realConnected(Y);
		onMsg serverDispatch : serverDispatch(start, _, _) -> selfMsg localCmd : localCmd(start)
		//TODO. Change plan on start message to clean the floor (mock),
		//under the assumption that conditions are always verified.
	]
	transition
		whenTime 100 -> waitForCommands
		whenMsg localCmd -> floorClean
	
	Plan floorClean [
		println("cleaning floor...");
		//Sending to baseSoffrittiRobot a forward robotCmd
		[ !? soffrittiConnected(true) ] forward basesoffrittirobot -m robotCmd : robotCmd(w, 800)
	]
	transition 
		whenTime 1000 -> turnAround
		//whenMsg cmd -> commandArrived
		whenMsg serverDispatch -> commandArrived
	finally repeatPlan
		
	Plan turnAround [
		println("turn around");
		//Sending to baseSoffrittiRobot a turn right robotCmd
		[ !? soffrittiConnected(true) ] forward basesoffrittirobot -m robotCmd : robotCmd(d, 300)
	]
	transition
		whenTime 1000 -> floorClean
		//whenMsg cmd -> commandArrived
		whenMsg serverDispatch -> commandArrived
	finally repeatPlan
	
	Plan commandArrived resumeLastPlan [
		println("command arrived while cleaning");
		//onMsg cmd : cmd(stopp) -> selfMsg localCmd : localCmd(stopp)
		onMsg serverDispatch : serverDispatch(stopp, _, _) -> selfMsg localCmd : localCmd(stopp)
		//When a stopCmd arrives, a Halt command is sent to Soffritti
	]
	transition
		whenTime 1000 -> floorClean
		whenMsg localCmd -> stopPlan
	
	Plan stopPlan [
		println("cleaning stopped");
		// Send to baseSoffrittiRobot an halt robotCmd
		[ !? soffrittiConnected(true) ] forward basesoffrittirobot -m robotCmd : robotCmd(h, unknown)
	]
	switchTo waitForCommands
	
}

//[R-FloorClean]
QActor basesoffrittirobot context ctxFinalTask {
	Plan init normal [
		//Soffrittisonar QActor is not necessary anymore, as this initialization also listens for
		//events from the virtual environment.
		javaRun it.unibo.utils.clientTcp.initClientConn("localhost","8999");
		delay 1000;
		println("soffritti start")
	] switchTo waitForCommands
	
	Plan waitForCommands [
		println("robot: waiting for commands")
	] transition stopAfter 3600000
		whenMsg robotCmd -> handleMessage
	finally repeatPlan
	
	Plan handleMessage resumeLastPlan [
		onMsg robotCmd : robotCmd(w, TIME) -> javaRun it.unibo.utils.clientTcp.soffrittiCommand("moveForward", TIME);
		onMsg robotCmd : robotCmd(s, TIME) -> javaRun it.unibo.utils.clientTcp.soffrittiCommand("moveBackward", TIME);
		onMsg robotCmd : robotCmd(a, TIME) -> javaRun it.unibo.utils.clientTcp.soffrittiCommand("turnLeft", TIME);
		onMsg robotCmd : robotCmd(d, TIME) -> javaRun it.unibo.utils.clientTcp.soffrittiCommand("turnRight", TIME);
		onMsg robotCmd : robotCmd(h, TIME) -> javaRun it.unibo.utils.clientTcp.sendMsg("{ 'type': 'alarm' }")
	]
}


//Test actor
QActor testsonar context ctxFinalTask {
	Plan init normal [
		println("testsonar start")
	] switchTo generateEvents
	
	Plan generateEvents [
		delay 3000
//		println("testsonar emitting sonarEvent");
//		emit sonarEvent : sonar(50);
//		delay 2000;
//		println("testsonar emitting collisionEvent");
//		emit collisionEvent : collision(x)
	]
}
