package it.unibo.custom.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import it.unibo.qactors.akka.QActor;

public class temperatureMockGui extends JPanel implements ChangeListener{
	
	private static final long serialVersionUID = 1L;
	private static temperatureMockGui currentTemperatureGui = null;
	public static JFrame frame = new JFrame();
	private static JSlider tempSlider = new JSlider(JSlider.VERTICAL,-10, 50, 20);
	
	private static QActor actor;

	public static synchronized temperatureMockGui createCustomTemperatureGui(QActor myActor) { //Factory method
    	if (currentTemperatureGui == null ) currentTemperatureGui = new temperatureMockGui(myActor);
    	return currentTemperatureGui;
    }
	
	public static void sendTemperature(QActor myActor) {
		int value = tempSlider.getValue();
		myActor.emit("temperatureEvent", "temperatureEvent("+value+")");
	}
    
    public temperatureMockGui(QActor myActor) {
    	actor = myActor;
    	tempSlider.addChangeListener(this);
    	tempSlider.setMajorTickSpacing(5);
    	tempSlider.setMinorTickSpacing(1);
    	tempSlider.setPaintTicks(true);
    	tempSlider.setPaintLabels(true);
    	this.add(tempSlider);
    	
        frame.setTitle("TemperatureMockGui");
        frame.add(this);        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 250);
        frame.setLocation(600, 300);
        frame.setVisible(true);
    }
    
    // For testing
    public static void main(String[] args) throws InterruptedException
    {
    	temperatureMockGui temp = new temperatureMockGui(null);
    	Thread.sleep(30000);
    }

	@Override
	public void stateChanged(ChangeEvent e) {
		if(actor!=null) {
         	actor.emit("temperatureEvent", "temperatureEvent("+tempSlider.getValue()+")");
         }
		 else System.out.println("newValue: " + tempSlider.getValue());
	}
	
}
