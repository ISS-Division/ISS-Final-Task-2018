package it.unibo.custom.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import it.unibo.qactors.akka.QActor;

public class timeMockGui extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private static timeMockGui currentTemperatureGui = null;
	public static JFrame frame = new JFrame();
	private static JComboBox<Integer> hourCombo = new JComboBox<>();
	private static JComboBox<Integer> minuteCombo = new JComboBox<>();
	
	private static QActor actor;

	public static synchronized timeMockGui createCustomTimeMockGui(QActor myActor) { //Factory method
    	if (currentTemperatureGui == null ) currentTemperatureGui = new timeMockGui(myActor);
    	return currentTemperatureGui;
    }
	
	public static void sendTime(QActor myActor) {
		myActor.emit("timeEvent", "timeEvent("+hourCombo.getSelectedItem()+", "+minuteCombo.getSelectedItem()+")");
	}
    
    public timeMockGui(QActor myActor) {
    	actor = myActor;
    	
    	for (int i=0; i<24; i++) {
    		hourCombo.addItem(i);
    	}
    	hourCombo.addActionListener(this);
    	for (int i=0; i<60; i++) {
    		minuteCombo.addItem(i);
    	}
    	minuteCombo.addActionListener(this);
    	this.add(hourCombo);
    	this.add(minuteCombo);
    	
        frame.setTitle("TimeMockGui");
        frame.add(this);        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(200, 60);
        frame.setLocation(800, 400);
        frame.setVisible(true);
    }
    
    // For testing
    public static void main(String[] args) throws InterruptedException
    {
    	timeMockGui temp = new timeMockGui(null);
    	Thread.sleep(30000);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if(actor!=null) {
			actor.emit("timeEvent", "timeEvent("+hourCombo.getSelectedItem()+", "+minuteCombo.getSelectedItem()+")");
         }
		 else System.out.println("newValue: " + hourCombo.getSelectedItem()+", "+minuteCombo.getSelectedItem());
	}
	
}
