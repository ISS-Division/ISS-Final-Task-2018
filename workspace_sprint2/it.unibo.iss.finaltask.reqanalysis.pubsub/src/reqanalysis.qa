System reqanalysis

Event userCmd : userCmd(X) //[X=start|stopp] //Commands from the user
Dispatch robotCmd : robotCmd(X, TIME) //Commands to the robot
Dispatch cmd : cmd(X) //Internal system commands

Event temperatureEvent : temperatureEvent(TEMP) //Used for the communication w/ the temperature provider
Event timeEvent : timeEvent(HOURS, MINUTES) //Used for the communication w/ the time provider

Event sonarEvent : sonar(DISTANCE)
Dispatch sonarDispatch : sonar(DISTANCE)
Event collisionEvent : collision(X)
Dispatch collisionDispatch : collision(X)

Dispatch localCmd : localCmd(X) //Used for self messages

pubSubServer "tcp://localhost:1883"

Context ctxFinalTask ip [ host="localhost" port=8570 ]
EventHandler collisionevh for collisionEvent {
	forwardEvent soffrittisonar -m collisionDispatch
};
EventHandler sonarevh for sonarEvent {
	forwardEvent soffrittisonar -m sonarDispatch
};

//This is needed considering the requirements of having both a real and a virtual robot.
//This actor provides behavioral bindings between a robot and its led.
QActor usercmdinterpreter context ctxFinalTask -pubsub {
	Rules {
		//[R-TimeOk and R-TempOk]
		limitTemperatureValue(25).
//		currentTemperature(25).
		lowerTimeBound(7, 00).
		upperTimeBound(10, 00).
//		currentTime(7, 00).

		eval( le, X, X ) :- !. 
		eval( le, X, V ) :- eval( lt, X , V ) .
			
		checkTemperature(TEMP) :-
			limitTemperatureValue(L), 
			eval(le, TEMP, L).
			
		checkTime(CH, CM) :-
			upperTimeBound(UH, UM), 
			lowerTimeBound(LH, LM), 
    		timeBeforeEqualThan(CH, CM, UH, UM), 
    		timeAfterEqualThan(CH, CM, LH, LM).
		
		evalTemperature(_) :- 
			currentTemperature(CUR), 
			checkTemperature(CUR).
		evalTime(_) :- 
			currentTime(CH, CM), 
			checkTime(CH, CM).

		timeBeforeEqualThan(H, M, H, M2) :- eval(le, M, M2), !.
		timeBeforeEqualThan(H, _, H2, _) :- eval(lt, H, H2), !.

		timeAfterEqualThan(H, M, H2, M2) :- timeBeforeEqualThan(H2, M2, H, M).

		evalConditions(_) :- evalTemperature(_), evalTime(_).
					
	}
	
	Plan init normal [
		println("usercmdinterpreter init")
	] switchTo waitForEvents
	
	Plan waitForEvents [
		println("usercmdinterpreter: waiting for events")
	] transition stopAfter 3600000
		whenEvent userCmd -> handleEvent,
		//Reactive to temperature or time changes
		//[R-TimeKo/R-TempKo]
		whenEvent timeEvent -> handleNewData,
		whenEvent temperatureEvent -> handleNewData
	finally repeatPlan
	
	Plan handleEvent resumeLastPlan [
		onEvent userCmd : userCmd(start) -> {
			[ !? robot(started) ] println("robot already started")
			else
			selfMsg localCmd : localCmd(start)	
		};
		onEvent userCmd : userCmd(stopp) -> {
				[ !? robot(started) ] {
					println("stopping robot");
					removeRule robot(started);
					forward ledcontrol -m cmd : cmd(stopp);
					forward robotcontrol -m cmd : cmd(stopp)
					}			
			}
	] transition
		whenTime 100 -> waitForEvents
		whenMsg localCmd -> askForTemperature
	
	Plan askForTemperature [
		forward temperatureprovider -m cmd : cmd(tempreq)
	] transition 
		whenTime 3000 -> waitForEvents //Maximum wait for temperature
		whenEvent temperatureEvent -> setTemperature
		
	Plan setTemperature [
		onEvent temperatureEvent : temperatureEvent(TEMP) -> 
			demo replaceRule(currentTemperature(_), currentTemperature(TEMP))
	] switchTo askForTime
	
	Plan askForTime [
		forward timeprovider -m cmd : cmd(timereq)
	] transition 
		whenTime 3000 -> waitForEvents //Maximum wait for time
		whenEvent timeEvent -> setTime 
	
	Plan setTime [
		onEvent timeEvent : timeEvent(HOURS, MINUTES) -> 
			demo replaceRule(currentTime(_, _), currentTime(HOURS, MINUTES))
	] switchTo evalConditions
		
	Plan evalConditions [
		[ !? evalConditions(_) ] {
			println("usercmdintepreter: conditions satisfied, sending start");
			addRule robot(started);
			forward ledcontrol -m cmd : cmd(start);
			forward robotcontrol -m cmd : cmd(start)
		} else
		println("usercmdinterpreter: conditions NOT satisfied")
	] switchTo waitForEvents
	
	Plan handleNewData resumeLastPlan [
		println("usercmdintepreter: sensor update arrived");
		onEvent timeEvent : timeEvent(H, M) -> demo replaceRule(currentTime(_, _), currentTime(H, M));
		onEvent temperatureEvent : temperatureEvent(TEMP) -> demo replaceRule(currentTemperature(_), currentTemperature(TEMP));
		[!? evalConditions(_)] {
			println("usercmdinterpreter: conditions still satisfied")
		} else {
			println("usercmdinterpreter: conditions not satisfied anymore");
			[ !? robot(started) ] {
					println("stopping robot");
					removeRule robot(started);
					forward ledcontrol -m cmd : cmd(stopp);
					forward robotcontrol -m cmd : cmd(stopp)
					}		
		}
	]
}

//[R-TempOk/R-TempKo]
//This actor provides temperature when requested. It is up to the designer to eventually implement
//a push system. It can also act as broker for different real providers.
//Moreover, this actor can both query an external service for temperature retrieval and store it for
//reuse in case the temperature service is temporarily offline.
QActor temperatureprovider context ctxFinalTask {
	Plan init normal [
		println("temperature provider init")
	] switchTo waitForMessage
	
	Plan waitForMessage [
		println("temperatureprovider: waiting for messages")
	] transition stopAfter 3600000
		whenMsg cmd -> fetchTemperature
	finally repeatPlan
	
	Plan fetchTemperature resumeLastPlan [
		//TODO: fetch the temperature from a given provider
		emit temperatureEvent : temperatureEvent(25);
		
		//TEST
		delay 2000;
		emit temperatureEvent : temperatureEvent(24);
		delay 3000;
		emit temperatureEvent : temperatureEvent(26)
	]
}

//[R-TimeOk/R-TimeKo]
//This actor provides time when requested. It is up to the designer to eventually implement
//a push system. It can also act as broker for different real providers.
//Moreover, this actor can both query an external clock service (and store it for
//reuse in case the clock service is temporarily offline).
QActor timeprovider context ctxFinalTask {
	Plan init normal [
		println("time provider init")
	] switchTo waitForMessage
	
	Plan waitForMessage [
		println("timeprovider: waiting for messages")
	] transition stopAfter 3600000
		whenMsg cmd -> fetchTime
	finally repeatPlan
	
	Plan fetchTime resumeLastPlan [
		//TODO: fetch the time from a given provider
		emit timeEvent : timeEvent(8, 00)
	]
}

////[R-FloorClean - probAnalysis, led only]
//QActor model context ctxFinalTask {
//	Rules{
//		 	changedModelAction(leds, led1, V  ):-
// 				emitevent(outputCtrlEvent, model(leds, led1, V) ).
//	}
//	
//	Plan init normal [
//		println("model init");
//		demo consult("./resourceModel.pl")  //contains the models and related rules
//	] switchTo waitForCommands
//	
//	Plan waitForCommands [
//		println("model: waiting for commands")
//	] transition stopAfter 3600000
//     	whenMsg inputCtrlDispatch -> handleInput
//    finally repeatPlan
//		
//	Plan handleInput resumeLastPlan [    	
//		onMsg inputCtrlDispatch : model( CATEG, NAME, VALUE ) ->  //change the model
//    		demo changeModelItem( CATEG, NAME, VALUE );
//    	println("changeModelItem")
//	]
//}

//[R-BlinkLed/R-BlinkHue]
QActor ledcontrol context ctxFinalTask {
	Plan init normal [         
   		println("ledcontrol init") 
   	]
   	switchTo waitForCommand     
  
    Plan waitForCommand[]  
    transition stopAfter 3600000 
     	whenMsg cmd -> startPlan
    finally repeatPlan	
   	 
	Plan startPlan resumeLastPlan[
		onMsg cmd : cmd(start) -> selfMsg localCmd : localCmd(start)
	]
	transition 
		whenTime 100 -> waitForCommand
		whenMsg localCmd -> blinkOn
		
	Plan blinkOn [
		println("led on")
	]
	transition 
		whenTime 500 -> blinkOff
		whenMsg cmd -> commandArrived
	finally repeatPlan
	
	Plan blinkOff [
		println("led off")
	]
	transition
		whenTime 500 -> blinkOn
		whenMsg cmd -> commandArrived
	finally repeatPlan
	
	Plan commandArrived resumeLastPlan [
		println("command arrived while blinking");
		onMsg cmd : cmd(stopp) -> selfMsg localCmd : localCmd(stopp)
	]
	transition
		whenTime 100 -> blinkOff //Should not happen
		whenMsg localCmd -> stopPlan
	
	Plan stopPlan [
		println("led off");
		println("blinking stopped")
	]
	switchTo waitForCommand
}


//[R-FloorClean]
//This QActor shows, just like ledcontrol, shows how to be reactive to commands
//while doing some work, as this is required.
QActor robotcontrol context ctxFinalTask {
	Plan init normal [
		println("robotcontrol init")
	] switchTo waitForCommands
	
	Plan waitForCommands [
		println("robotcontrol: waiting for commands")
	] transition stopAfter 3600000
		whenMsg cmd -> startPlan
	finally repeatPlan
		
	Plan startPlan [
		println("command arrived");
		onMsg cmd : cmd(start) -> selfMsg localCmd : localCmd(start)
		//TODO. Change plan on start message to clean the floor (mock),
		//under the assumption that conditions are always verified.
	]
	transition
		whenTime 100 -> waitForCommands
		whenMsg localCmd -> floorClean
	
	Plan floorClean [
		println("cleaning floor...")
	]
	transition stopAfter 3600000
		whenMsg cmd -> commandArrived
	
	Plan commandArrived resumeLastPlan [
		println("command arrived while cleaning");
		onMsg cmd : cmd(stopp) -> selfMsg localCmd : localCmd(stopp)
		//On Stop command, send an halt.
	]
	transition
		whenTime 100 -> floorClean
		whenMsg localCmd -> stopPlan
	
	Plan stopPlan [
		println("cleaning stopped, eventually do stuff");
		forward basesoffrittirobot -m robotCmd : robotCmd(h, unknown)
	]
	switchTo waitForCommands
	
}

//[R-FloorClean]
QActor basesoffrittirobot context ctxFinalTask {
	Plan init normal [
		println("robot init")
	] switchTo waitForCommands
	
	Plan waitForCommands [
		println("robot: waiting for commands")
	] transition stopAfter 3600000
		whenMsg robotCmd -> handleMessage
	finally repeatPlan
	
	Plan handleMessage resumeLastPlan [
		onMsg robotCmd : robotCmd(w, TIME) -> /*println ("forward");*/ addRule go(w, TIME); //TODO, for testing purposes
		onMsg robotCmd : robotCmd(a, TIME) -> /*println ("left");*/ addRule go(a, TIME); //TODO, for testing purposes
		onMsg robotCmd : robotCmd(s, TIME) -> /*println ("backward");*/ addRule go(s, TIME); //TODO, for testing purposes
		onMsg robotCmd : robotCmd(d, TIME) -> /*println ("right");*/ addRule go(d, TIME); //TODO, for testing purposes
		onMsg robotCmd : robotCmd(h, TIME) -> {println ("halt"); addRule go(h)} //TODO, for testing purposes
	]
}

QActor soffrittisonar context ctxFinalTask {
	Plan init normal [
		println("soffritti sonar start")
	] switchTo waitForEvents
	
	Plan waitForEvents [
		println("sonar: waiting for messages")
	] transition stopAfter 3600000
		whenMsg sonarDispatch -> handleMessage,
		whenMsg collisionDispatch -> handleMessage
	finally repeatPlan
	
	Plan handleMessage resumeLastPlan [
		printCurrentMessage
	] 
}