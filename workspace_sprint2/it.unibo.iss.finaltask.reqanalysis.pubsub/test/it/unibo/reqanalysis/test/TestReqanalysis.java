package it.unibo.reqanalysis.test;

import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.ctxFinalTask.MainCtxFinalTask;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.SolveInfo;

public class TestReqanalysis {
	private QActor robotcontrol = null;
	private QActor basesoffrittirobot = null;
	private QActor guisimulator = null;
	private QActor soffrittisonar = null;
	private QActor testsonar = null;

	@Before
	public void systemSetUp() throws Exception {
		System.out.println("SystemSetUp starts");
		MainCtxFinalTask.initTheContext();
		waitQActorsLogRunning();
	}

	@After
	public void terminate() {
		System.out.println("=== TEST TERMINATED ===");
	}

	@Test
	public void reqTestForward() throws InterruptedException {
		System.out.println("=== TEST STARTED ===");
		SolveInfo sol;

		try {
			robotcontrol.sendMsg("robotCmd", "basesoffrittirobot", "Dispatch", "robotCmd(w, 100)");
			Thread.sleep(200);
			sol = basesoffrittirobot.solveGoal("go(w, 100)");
			if (sol.isSuccess()) {
				assertTrue("", sol.isSuccess());
			} else {
				System.out.println("WARNING ... ");
				fail("reqTest STRANGE BEHAVIOUR");
			}
			basesoffrittirobot.removeRule("go(w, 100)");
		} catch (Exception e) {
			System.out.println("ERROR=" + e.getMessage());
			fail("reqTest " + e.getMessage());
		}
	}
	
	@Test
	public void reqTestLeft() throws InterruptedException {
		System.out.println("=== TEST STARTED ===");
		SolveInfo sol;
		
		try {
			robotcontrol.sendMsg("robotCmd", "basesoffrittirobot", "Dispatch", "robotCmd(a, 100)");
			Thread.sleep(200);
			sol = basesoffrittirobot.solveGoal("go(a, 100)");
			if (sol.isSuccess()) {
				assertTrue("", sol.isSuccess());
			} else {
				System.out.println("WARNING ... ");
				fail("reqTest STRANGE BEHAVIOUR");
			}
			basesoffrittirobot.removeRule("go(a, 100)");
		} catch (Exception e) {
			System.out.println("ERROR=" + e.getMessage());
			fail("reqTest " + e.getMessage());
		}
	}

	@Test
	public void reqTestBackward() throws InterruptedException {
		System.out.println("=== TEST STARTED ===");
		SolveInfo sol;
		
		try {
			robotcontrol.sendMsg("robotCmd", "basesoffrittirobot", "Dispatch", "robotCmd(s, 100)");
			Thread.sleep(200);
			sol = basesoffrittirobot.solveGoal("go(s,100)");
			if (sol.isSuccess()) {
				assertTrue("", sol.isSuccess());
			} else {
				System.out.println("WARNING ... ");
				fail("reqTest STRANGE BEHAVIOUR");
			}
			basesoffrittirobot.removeRule("go(s,100)");
		} catch (Exception e) {
			System.out.println("ERROR=" + e.getMessage());
			fail("reqTest " + e.getMessage());
		}
	}
	
	@Test
	public void reqTestRight() throws InterruptedException {
		System.out.println("=== TEST STARTED ===");
		SolveInfo sol;
		
		try {
			robotcontrol.sendMsg("robotCmd", "basesoffrittirobot", "Dispatch", "robotCmd(d, 100)");
			Thread.sleep(200);
			sol = basesoffrittirobot.solveGoal("go(d, 100)");
			if (sol.isSuccess()) {
				assertTrue("", sol.isSuccess());
			} else {
				System.out.println("WARNING ... ");
				fail("reqTest STRANGE BEHAVIOUR");
			}
			basesoffrittirobot.removeRule("go(d,100)");
		} catch (Exception e) {
			System.out.println("ERROR=" + e.getMessage());
			fail("reqTest " + e.getMessage());
		}
	}
	
	@Test
	public void reqTestHalt() throws InterruptedException {
		System.out.println("=== TEST STARTED ===");
		SolveInfo sol;
		
		try {
			robotcontrol.sendMsg("robotCmd", "basesoffrittirobot", "Dispatch", "robotCmd(h, unknown)");
			Thread.sleep(200);
			sol = basesoffrittirobot.solveGoal("go(h)");
			if (sol.isSuccess()) {
				assertTrue("", sol.isSuccess());
			} else {
				System.out.println("WARNING ... ");
				fail("reqTest STRANGE BEHAVIOUR");
			}
			basesoffrittirobot.removeRule("go(h)");
		} catch (Exception e) {
			System.out.println("ERROR=" + e.getMessage());
			fail("reqTest " + e.getMessage());
		}
	}
	
	@Test
	public void reqTestStartStopp() throws InterruptedException {
		System.out.println("=== TEST STARTED ===");
		SolveInfo sol;
		
		try {
			guisimulator.emit("userCmd", "userCmd(start)");
			Thread.sleep(1000);
			guisimulator.emit("userCmd", "userCmd(stopp)");
			Thread.sleep(1000);
			sol = basesoffrittirobot.solveGoal("go(h)");
			if (sol.isSuccess()) {
				assertTrue("", sol.isSuccess());
			} else {
				System.out.println("WARNING ... ");
				fail("reqTest STRANGE BEHAVIOUR");
			}
			basesoffrittirobot.removeRule("go(h)");
		} catch (Exception e) {
			System.out.println("ERROR=" + e.getMessage());
			fail("reqTest " + e.getMessage());
		}
	}

	protected void waitQActorsLogRunning() {
		try {
			System.out.println("waitQAcotrsLogRunning ... ");
			while (robotcontrol == null || basesoffrittirobot == null || guisimulator == null || soffrittisonar == null
					|| testsonar == null) {
				Thread.sleep(250);
				robotcontrol = QActorUtils.getQActor("robotcontrol_ctrl");
				basesoffrittirobot = QActorUtils.getQActor("basesoffrittirobot_ctrl");
				guisimulator = QActorUtils.getQActor("guisimulator_ctrl");
				soffrittisonar = QActorUtils.getQActor("soffrittisonar_ctrl");
				testsonar = QActorUtils.getQActor("testsonar_ctrl");
			}
			System.out.println("robotcontrol RUNNING: " + robotcontrol);
			System.out.println("basesoffrittirobot RUNNING: " + basesoffrittirobot);

		} catch (Exception e) {
			System.out.println("waitForRoverRunning fails " + e.getMessage());
		}
	}

}
