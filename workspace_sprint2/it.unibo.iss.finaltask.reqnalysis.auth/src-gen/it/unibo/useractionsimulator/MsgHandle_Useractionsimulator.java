/* Generated by AN DISI Unibo */ 
package it.unibo.useractionsimulator;
import it.unibo.qactors.QActorContext;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.akka.QActorMsgQueue;

public class MsgHandle_Useractionsimulator extends QActorMsgQueue{
	public MsgHandle_Useractionsimulator(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  {
		super(actorId, myCtx, outEnvView);
	}
}
