
#include "SR04.h"
#define LED 2
#define LEFT_MOTOR1 6
#define LEFT_MOTOR2 7
#define RIGHT_MOTOR1 8
#define RIGHT_MOTOR2 9
#define EN 11
#define BUTTON 5

#define TRIG_PIN 13
#define ECHO_PIN 12
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);
long a;

void setup() {
  Serial.begin(9600);

  pinMode(LED, OUTPUT);
  pinMode(LEFT_MOTOR1, OUTPUT);
  pinMode(RIGHT_MOTOR1, OUTPUT);
  pinMode(LEFT_MOTOR2, OUTPUT);
  pinMode(RIGHT_MOTOR2, OUTPUT);
  pinMode(EN, OUTPUT);
  pinMode(BUTTON, INPUT);
  
  digitalWrite(LEFT_MOTOR1, 1);
  digitalWrite(RIGHT_MOTOR1, 1);
  digitalWrite(LEFT_MOTOR2, 0);
  digitalWrite(RIGHT_MOTOR2, 0);
  
  digitalWrite(LED, 1);
  delay(500);
  digitalWrite(LED, 0);
  delay(500);
}

void turnR(int power) {
  digitalWrite(RIGHT_MOTOR1, 1);
  digitalWrite(RIGHT_MOTOR2, 0);
  digitalWrite(LEFT_MOTOR1, 0);
  digitalWrite(LEFT_MOTOR2, 1);

  analogWrite(EN, power); 
  delay(500);
}
  
void turnL(int power) {
  digitalWrite(RIGHT_MOTOR1, 0);
  digitalWrite(RIGHT_MOTOR2, 1);
  digitalWrite(LEFT_MOTOR1, 1);
  digitalWrite(LEFT_MOTOR2, 0);
  
  analogWrite(EN, power);
  delay(500);
}
  
void forward(int power) {
  digitalWrite(RIGHT_MOTOR1, 1);
  digitalWrite(RIGHT_MOTOR2, 0);
  digitalWrite(LEFT_MOTOR1, 1);
  digitalWrite(LEFT_MOTOR2, 0);
  
  analogWrite(EN, power);
  delay(500);
}
  
void back(int power) {
  digitalWrite(RIGHT_MOTOR1, 0);
  digitalWrite(RIGHT_MOTOR2, 1);
  digitalWrite(LEFT_MOTOR1, 0);
  digitalWrite(LEFT_MOTOR2, 1);

  analogWrite(EN, power);
  delay(500);
}
  
void stop() {
  digitalWrite(RIGHT_MOTOR1, 0);
  digitalWrite(RIGHT_MOTOR2, 0);
  digitalWrite(LEFT_MOTOR1, 0);
  digitalWrite(LEFT_MOTOR2, 0);

  analogWrite(EN, 0);
  delay(500);
}
  
void remoteCommandExecutor() {
  String duration, power;
  int dur, pwr;
  char command;
  
  if (Serial.available() > 0) {
    command = Serial.readStringUntil(',')[0];
    duration = Serial.readStringUntil(',');
    power = Serial.readStringUntil('\n');

    pwr = power.toInt();
    if (pwr > 255) {
      pwr = 255;
    }  
    
    switch(command){
      case 'w': //w
        forward(pwr);   
        break;
      case 's': //s
        back(pwr);
        break;
      case 'a':  //a
        turnL(pwr);
        break;
      case 'd': //d
        turnR(pwr);
        break;
      case 'h': //h
        stop();
        break;
      case 'b': //b 
        digitalWrite(LED, 1);
        break;
      case 'n': //n
        digitalWrite(LED, 0);
        break;
      default:
        stop();
    }
    
    dur = duration.toInt();
    if(dur > 0){
      delay(dur);
      if (command == 'b' || command == 'n'){
        digitalWrite(LED, 0);
      }
      else{
        stop();
      }
    }
  }
}

void sonarDetection() {
  a=sr04.DistanceAvg();
  Serial.println(a); 
}

void loop() {
  int button_status = digitalRead(BUTTON);
  if (button_status == 0) {
    delay(50);
  }
  else {
    digitalWrite(LED, 1);
    digitalWrite(LEFT_MOTOR1, 1);
    digitalWrite(LEFT_MOTOR2, 0);
    digitalWrite(RIGHT_MOTOR1, 1);
    digitalWrite(RIGHT_MOTOR2, 0);
    analogWrite(EN, 255);
    
    delay(2000);
    digitalWrite(LED, 0);
    analogWrite(EN, 0);
  }
  
  //sonarDetection();
  remoteCommandExecutor(); 
}
