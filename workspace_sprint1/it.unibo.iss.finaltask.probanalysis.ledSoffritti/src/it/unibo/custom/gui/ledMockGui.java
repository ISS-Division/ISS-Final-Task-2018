package it.unibo.custom.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import it.unibo.qactors.akka.QActor;

public class ledMockGui extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private static ledMockGui currentLedGui = null;
	public static Color ledColor = Color.WHITE;
	public static final boolean ledGuiOn  = true;
	public static final boolean ledGuiOff = false;
	public static boolean ledStatus = ledGuiOff;
	public static JFrame frame = new JFrame();

	public static synchronized ledMockGui createCustomLedGui(QActor myActor) { //Factory method
    	if (currentLedGui == null ) currentLedGui = new ledMockGui(myActor);
    	return currentLedGui;
    }
	
    public static void setLed(QActor myActor, String value) {
      	currentLedGui.setLedGui( value.equals("on") ? ledGuiOn : ledGuiOff );
    }   
    
    public ledMockGui(QActor myActor) {
        frame.setTitle("LedMockGui");
        frame.add(this);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 500);
        frame.setVisible(true);
    }
	
    public void paintComponent(Graphics g) {
        if(ledStatus)
            ledColor = Color.RED;
        else
            ledColor = Color.WHITE;
        g.setColor(ledColor);
        g.fillOval(frame.getWidth()/2-125, frame.getWidth()/2-125, 250, 250);
    }

    public void setLedGui(boolean status) {
    	ledStatus = status;
    	repaint();
    } 
    
    
    // For testing
    public static void main(String[] args) throws InterruptedException
    {
        ledMockGui led = new ledMockGui(null);
        for( int i=1; i<=10; i++) {
			Thread.sleep(1000);
			led.setLedGui(ledGuiOn);
			Thread.sleep(1000);
			led.setLedGui(ledGuiOff);
    	}
    }
	
}
