%====================================================================================
% Context ctxFinalTask  SYSTEM-configuration: file it.unibo.ctxFinalTask.reqanalysis.pl 
%====================================================================================
context(ctxfinaltask, "localhost",  "TCP", "8570" ).  		 
%%% -------------------------------------------
qactor( human , ctxfinaltask, "it.unibo.human.MsgHandle_Human"   ). %%store msgs 
qactor( human_ctrl , ctxfinaltask, "it.unibo.human.Human"   ). %%control-driven 
qactor( usercmdinterpreter , ctxfinaltask, "it.unibo.usercmdinterpreter.MsgHandle_Usercmdinterpreter"   ). %%store msgs 
qactor( usercmdinterpreter_ctrl , ctxfinaltask, "it.unibo.usercmdinterpreter.Usercmdinterpreter"   ). %%control-driven 
qactor( ledcontroller , ctxfinaltask, "it.unibo.ledcontroller.MsgHandle_Ledcontroller"   ). %%store msgs 
qactor( ledcontroller_ctrl , ctxfinaltask, "it.unibo.ledcontroller.Ledcontroller"   ). %%control-driven 
qactor( robotcontrol , ctxfinaltask, "it.unibo.robotcontrol.MsgHandle_Robotcontrol"   ). %%store msgs 
qactor( robotcontrol_ctrl , ctxfinaltask, "it.unibo.robotcontrol.Robotcontrol"   ). %%control-driven 
qactor( basesoffrittirobot , ctxfinaltask, "it.unibo.basesoffrittirobot.MsgHandle_Basesoffrittirobot"   ). %%store msgs 
qactor( basesoffrittirobot_ctrl , ctxfinaltask, "it.unibo.basesoffrittirobot.Basesoffrittirobot"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(collisionevh,ctxfinaltask,"it.unibo.ctxFinalTask.Collisionevh","collisionEvent").  
eventhandler(sonarevh,ctxfinaltask,"it.unibo.ctxFinalTask.Sonarevh","sonarEvent").  
eventhandler(webadapter,ctxfinaltask,"it.unibo.ctxFinalTask.Webadapter","usercmd").  
%%% -------------------------------------------

