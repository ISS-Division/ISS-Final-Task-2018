#!/bin/bash

for D in `find . -regex '^\./it.unibo.iss.finaltask.*' -maxdepth 1 -mindepth 1 -type d`
do
	( printf "============================================================================\n🍺 $D\n============================================================================" && cd $D && gradle -b build_ctxFinalTask.gradle eclipse && cd ..)
done
