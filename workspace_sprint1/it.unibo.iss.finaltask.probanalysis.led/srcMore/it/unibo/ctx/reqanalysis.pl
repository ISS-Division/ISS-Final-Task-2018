%====================================================================================
% Context ctx  SYSTEM-configuration: file it.unibo.ctx.reqanalysis.pl 
%====================================================================================
context(ctx, "localhost",  "TCP", "8570" ).  		 
%%% -------------------------------------------
qactor( human , ctx, "it.unibo.human.MsgHandle_Human"   ). %%store msgs 
qactor( human_ctrl , ctx, "it.unibo.human.Human"   ). %%control-driven 
qactor( control , ctx, "it.unibo.control.MsgHandle_Control"   ). %%store msgs 
qactor( control_ctrl , ctx, "it.unibo.control.Control"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

