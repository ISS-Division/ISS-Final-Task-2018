%====================================================================================
% Context ctxFinalTask  SYSTEM-configuration: file it.unibo.ctxFinalTask.probanalysis.pl 
%====================================================================================
context(ctxfinaltask, "localhost",  "TCP", "8560" ).  		 
%%% -------------------------------------------
qactor( human , ctxfinaltask, "it.unibo.human.MsgHandle_Human"   ). %%store msgs 
qactor( human_ctrl , ctxfinaltask, "it.unibo.human.Human"   ). %%control-driven 
qactor( control , ctxfinaltask, "it.unibo.control.MsgHandle_Control"   ). %%store msgs 
qactor( control_ctrl , ctxfinaltask, "it.unibo.control.Control"   ). %%control-driven 
qactor( ledmockgui , ctxfinaltask, "it.unibo.ledmockgui.MsgHandle_Ledmockgui"   ). %%store msgs 
qactor( ledmockgui_ctrl , ctxfinaltask, "it.unibo.ledmockgui.Ledmockgui"   ). %%control-driven 
qactor( virtualrobot , ctxfinaltask, "it.unibo.virtualrobot.MsgHandle_Virtualrobot"   ). %%store msgs 
qactor( virtualrobot_ctrl , ctxfinaltask, "it.unibo.virtualrobot.Virtualrobot"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evadapter,ctxfinaltask,"it.unibo.ctxFinalTask.Evadapter","inputCtrlEvent").  
eventhandler(webadapter,ctxfinaltask,"it.unibo.ctxFinalTask.Webadapter","usercmd").  
%%% -------------------------------------------

