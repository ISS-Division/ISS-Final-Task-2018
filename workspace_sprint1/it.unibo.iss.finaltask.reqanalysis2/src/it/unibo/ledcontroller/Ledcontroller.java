/* Generated by AN DISI Unibo */ 
/*
This code is generated only ONCE
*/
package it.unibo.ledcontroller;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Ledcontroller extends AbstractLedcontroller { 
	public Ledcontroller(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
		super(actorId, myCtx, outEnvView);
	}
/*
 * ADDED BY THE APPLICATION DESIGNER	
 */
}
