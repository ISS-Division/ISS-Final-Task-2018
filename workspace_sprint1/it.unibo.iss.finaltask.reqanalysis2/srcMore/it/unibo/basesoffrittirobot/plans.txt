plan(1,init, sentence(true,move(print("robot init")),'','' ))
plan(1,waitForCommands, sentence(true,move(print("robot: waiting for commands")),'','' ))
plan(1,handleMessage, sentence(true,msgswitch(robotCmd, robotCmd( w,TIME) , addRule( go( w,TIME) )),'','' ))
plan(2,handleMessage, sentence(true,msgswitch(robotCmd, robotCmd( a,TIME) , addRule( go( a,TIME) )),'','' ))
plan(3,handleMessage, sentence(true,msgswitch(robotCmd, robotCmd( s,TIME) , addRule( go( s,TIME) )),'','' ))
plan(4,handleMessage, sentence(true,msgswitch(robotCmd, robotCmd( d,TIME) , addRule( go( d,TIME) )),'','' ))
plan(5,handleMessage, sentence(true,msgswitch(robotCmd, robotCmd( h,TIME) , not here StateMoveNormal),'','' ))
plan(6,handleMessage, sentence(true,move(resumeplan),'','' ))
