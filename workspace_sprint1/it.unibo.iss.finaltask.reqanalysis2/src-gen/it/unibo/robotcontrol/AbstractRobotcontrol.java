/* Generated by AN DISI Unibo */ 
package it.unibo.robotcontrol;
import it.unibo.qactors.PlanRepeat;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.StateExecMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.akka.QActor;
import it.unibo.qactors.StateFun;
import java.util.Stack;
import java.util.Hashtable;
import java.util.concurrent.Callable;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.qactors.action.ActorTimedAction;
public abstract class AbstractRobotcontrol extends QActor { 
	protected AsynchActionResult aar = null;
	protected boolean actionResult = true;
	protected alice.tuprolog.SolveInfo sol;
	protected String planFilePath    = null;
	protected String terminationEvId = "default";
	protected String parg="";
	protected boolean bres=false;
	protected IActorAction action;
	 
	
		protected static IOutputEnvView setTheEnv(IOutputEnvView outEnvView ){
			return outEnvView;
		}
		public AbstractRobotcontrol(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
			super(actorId, myCtx,  
			"./srcMore/it/unibo/robotcontrol/WorldTheory.pl",
			setTheEnv( outEnvView )  , "init");
			this.planFilePath = "./srcMore/it/unibo/robotcontrol/plans.txt";
	  	}
		@Override
		protected void doJob() throws Exception {
			String name  = getName().replace("_ctrl", "");
			mysupport = (IMsgQueue) QActorUtils.getQActor( name ); 
			initStateTable(); 
	 		initSensorSystem();
	 		history.push(stateTab.get( "init" ));
	  	 	autoSendStateExecMsg();
	  		//QActorContext.terminateQActorSystem(this);//todo
		} 	
		/* 
		* ------------------------------------------------------------
		* PLANS
		* ------------------------------------------------------------
		*/    
	    //genAkkaMshHandleStructure
	    protected void initStateTable(){  	
	    	stateTab.put("handleToutBuiltIn",handleToutBuiltIn);
	    	stateTab.put("init",init);
	    	stateTab.put("waitForCommands",waitForCommands);
	    	stateTab.put("startPlan",startPlan);
	    	stateTab.put("floorClean",floorClean);
	    	stateTab.put("commandArrived",commandArrived);
	    	stateTab.put("stopPlan",stopPlan);
	    }
	    StateFun handleToutBuiltIn = () -> {	
	    	try{	
	    		PlanRepeat pr = PlanRepeat.setUp("handleTout",-1);
	    		String myselfName = "handleToutBuiltIn";  
	    		println( "robotcontrol tout : stops");  
	    		repeatPlanNoTransition(pr,myselfName,"application_"+myselfName,false,false);
	    	}catch(Exception e_handleToutBuiltIn){  
	    		println( getName() + " plan=handleToutBuiltIn WARNING:" + e_handleToutBuiltIn.getMessage() );
	    		QActorContext.terminateQActorSystem(this); 
	    	}
	    };//handleToutBuiltIn
	    
	    StateFun init = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("init",-1);
	    	String myselfName = "init";  
	    	temporaryStr = "\"robotcontrol init\"";
	    	println( temporaryStr );  
	    	//switchTo waitForCommands
	        switchToPlanAsNextState(pr, myselfName, "robotcontrol_"+myselfName, 
	              "waitForCommands",false, false, null); 
	    }catch(Exception e_init){  
	    	 println( getName() + " plan=init WARNING:" + e_init.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//init
	    
	    StateFun waitForCommands = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp(getName()+"_waitForCommands",0);
	     pr.incNumIter(); 	
	    	String myselfName = "waitForCommands";  
	    	temporaryStr = "\"robotcontrol: waiting for commands\"";
	    	println( temporaryStr );  
	    	//bbb
	     msgTransition( pr,myselfName,"robotcontrol_"+myselfName,false,
	          new StateFun[]{stateTab.get("startPlan") }, 
	          new String[]{"true","M","cmd" },
	          3600000, "handleToutBuiltIn" );//msgTransition
	    }catch(Exception e_waitForCommands){  
	    	 println( getName() + " plan=waitForCommands WARNING:" + e_waitForCommands.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//waitForCommands
	    
	    StateFun startPlan = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("startPlan",-1);
	    	String myselfName = "startPlan";  
	    	temporaryStr = "\"command arrived\"";
	    	println( temporaryStr );  
	    	//onMsg 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("cmd(start)");
	    	if( currentMessage != null && currentMessage.msgId().equals("cmd") && 
	    		pengine.unify(curT, Term.createTerm("cmd(X)")) && 
	    		pengine.unify(curT, Term.createTerm( currentMessage.msgContent() ) )){ 
	    		String parg="localCmd(start)";
	    		/* SendDispatch */
	    		parg = updateVars(Term.createTerm("cmd(X)"),  Term.createTerm("cmd(start)"), 
	    			    		  					Term.createTerm(currentMessage.msgContent()), parg);
	    		if( parg != null ) sendMsg("localCmd",getNameNoCtrl(), QActorContext.dispatch, parg ); 
	    	}
	    	//bbb
	     msgTransition( pr,myselfName,"robotcontrol_"+myselfName,false,
	          new StateFun[]{stateTab.get("floorClean") }, 
	          new String[]{"true","M","localCmd" },
	          100, "waitForCommands" );//msgTransition
	    }catch(Exception e_startPlan){  
	    	 println( getName() + " plan=startPlan WARNING:" + e_startPlan.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//startPlan
	    
	    StateFun floorClean = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("floorClean",-1);
	    	String myselfName = "floorClean";  
	    	temporaryStr = "\"cleaning floor...\"";
	    	println( temporaryStr );  
	    	//bbb
	     msgTransition( pr,myselfName,"robotcontrol_"+myselfName,false,
	          new StateFun[]{stateTab.get("commandArrived") }, 
	          new String[]{"true","M","cmd" },
	          3600000, "handleToutBuiltIn" );//msgTransition
	    }catch(Exception e_floorClean){  
	    	 println( getName() + " plan=floorClean WARNING:" + e_floorClean.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//floorClean
	    
	    StateFun commandArrived = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("commandArrived",-1);
	    	String myselfName = "commandArrived";  
	    	temporaryStr = "\"command arrived while cleaning\"";
	    	println( temporaryStr );  
	    	//onMsg 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("cmd(stopp)");
	    	if( currentMessage != null && currentMessage.msgId().equals("cmd") && 
	    		pengine.unify(curT, Term.createTerm("cmd(X)")) && 
	    		pengine.unify(curT, Term.createTerm( currentMessage.msgContent() ) )){ 
	    		String parg="localCmd(stopp)";
	    		/* SendDispatch */
	    		parg = updateVars(Term.createTerm("cmd(X)"),  Term.createTerm("cmd(stopp)"), 
	    			    		  					Term.createTerm(currentMessage.msgContent()), parg);
	    		if( parg != null ) sendMsg("localCmd",getNameNoCtrl(), QActorContext.dispatch, parg ); 
	    	}
	    	//bbb
	     msgTransition( pr,myselfName,"robotcontrol_"+myselfName,true,
	          new StateFun[]{stateTab.get("stopPlan") }, 
	          new String[]{"true","M","localCmd" },
	          100, "floorClean" );//msgTransition
	    }catch(Exception e_commandArrived){  
	    	 println( getName() + " plan=commandArrived WARNING:" + e_commandArrived.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//commandArrived
	    
	    StateFun stopPlan = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("stopPlan",-1);
	    	String myselfName = "stopPlan";  
	    	temporaryStr = "\"cleaning stopped, eventually do stuff\"";
	    	println( temporaryStr );  
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"robotCmd(X,TIME)","robotCmd(h,unknown)", guardVars ).toString();
	    	sendMsg("robotCmd","basesoffrittirobot", QActorContext.dispatch, temporaryStr ); 
	    	//switchTo waitForCommands
	        switchToPlanAsNextState(pr, myselfName, "robotcontrol_"+myselfName, 
	              "waitForCommands",false, false, null); 
	    }catch(Exception e_stopPlan){  
	    	 println( getName() + " plan=stopPlan WARNING:" + e_stopPlan.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//stopPlan
	    
	    protected void initSensorSystem(){
	    	//doing nothing in a QActor
	    }
	
	}
